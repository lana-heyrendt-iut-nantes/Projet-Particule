package projet.echecmartien.librairie


const val TAILLEHORIZONTALE=4
const val TAILLEVERTICALE=8
var NOMBRECOUPSDAVANCE = 2

//BOUTON
var COULEURFONDBOUTTON = "#2970cf"
var COULEURBOUTTONRETOUR = "#ff5757"
var COULEURTEXTEBOUTTON = "#000000"
var COULEURTEXTEBOUTTONRETOUR = "#000000"
var COULEURBORDERBOUTTON = "#303030"

//REGLES DU JEUX
var NOMBREDECOUPMAX = 9

//PION
var COULEURGRANDPION = "#3366FF"
var COULEURMOYENPION = "#33CCFF"
var COULEURPETITPION = "#66FFFF"

//ACCESSIBILIT2
var BACKGROUND = "src/main/resources/Nathan.png"
val CHARGEMENT = "src/main/resources/loading.gif"
var TAILLEPOLICE = 22
var TAILLEPOLICETITRE = TAILLEPOLICE*1.25
var TAILLECASE = 90.0
var HAUTEURFENETRE = 950.0
var LARGEURFENETRE = 950.0
var COULEURPOLICE = "black"
var COULEURFOND = "#F9E4CF"
var OPACITY = 1.0

//PLATEAU
var COULEURGRILLE = "#F5EFEE"
var COULEURGRILLE2 = "#F1F5E2"
var COULEURJ1 = "#E1A29B"
var COULEURJ2 = "#ECD49D"
var COULEURCASEJ1 = "#d0eddc"
var COULEURCASEJ2 = "#f4d1b2"
var COULEURLIGNECENTRALE = "#FF0000"
var FLECHEJOUEUR1 = "src/main/resources/flechejoueur1.png"
var FLECHEJOUEUR2 = "src/main/resources/flechejoueur2.png"

enum class EnumPion(var valeur: String) {
    GRANDPION("G"),
    MOYENPION("M"),
    PETITPION("P"),
    LIBRE("  ");
}

class GeneralData {
    val tableau = Array(TAILLEHORIZONTALE) { Array(TAILLEVERTICALE) { EnumPion.LIBRE } }

    init {

        tableau[2][0]= EnumPion.MOYENPION
        tableau[1][1]= EnumPion.MOYENPION
        tableau[0][2]= EnumPion.MOYENPION
        tableau[3][5]= EnumPion.MOYENPION
        tableau[2][6]= EnumPion.MOYENPION
        tableau[1][7]= EnumPion.MOYENPION
        tableau[0][0]= EnumPion.GRANDPION
        tableau[1][0]= EnumPion.GRANDPION
        tableau[0][1]= EnumPion.GRANDPION
        tableau[3][6]= EnumPion.GRANDPION
        tableau[3][7]= EnumPion.GRANDPION
        tableau[2][7]= EnumPion.GRANDPION
        tableau[1][2]= EnumPion.PETITPION
        tableau[2][2]= EnumPion.PETITPION
        tableau[2][1]= EnumPion.PETITPION
        tableau[2][5]= EnumPion.PETITPION
        tableau[1][6]= EnumPion.PETITPION
        tableau[1][5]= EnumPion.PETITPION
    }
}

class DeplacementException(message: String = "Déplacement Illégal") : Exception(message)
class LoadingException(message: String = "le format est invalide") : Exception(message)



