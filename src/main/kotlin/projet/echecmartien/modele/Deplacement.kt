package projet.echecmartien.modele

import projet.echecmartien.librairie.DeplacementException
import kotlin.math.absoluteValue


/**
 * cette classe permet de tester les déplacements sur le plateau de jeu
 *
 */


class Deplacement(private val origine : Coordonnee, private val destination : Coordonnee) {

    /**
     * dans le constructeur la validité du déplacement dans la grille est testée
     *@throws DeplacementException si le déplacement n'est ni horizontal, ni vertical est ni diagonal
     * les autres cas lèvent une IllegalArgumentException (peut être mis en place avec "require")
     */
    init{
        if ((!estDiagonal()) && (!estVertical()) && (!estHorizontal())) throw DeplacementException("déplacement invalide")
    }

    /**
     * getter
     * @return la destination de ce déplacement
     */
    fun getDestination():Coordonnee = destination


    /**
     * getter
     * @return l'origine de ce déplacement
     */
    fun getOrigine():Coordonnee = origine

    /**
     *méthode qui permet de tester si le déplacement est horizontal
     * @return true si le déplacement est horizontal, false sinon
     */
    fun estHorizontal() : Boolean  = (origine.getY() == destination.getY())
    /**
     *méthode qui permet de tester si le déplacement est vertical
     * @return true si le déplacement est vertical, false sinon
     */
    fun estVertical(): Boolean = (origine.getX() == destination.getX())

    /**
     * méthode qui permet de tester si le déplacement est diagonal
     * @return true si le déplacement est diagonal, false sinon
     */
    fun estDiagonal():Boolean{
        val differenceX = (origine.getX() - destination.getX()).absoluteValue
        val differenceY = (origine.getY() - destination.getY()).absoluteValue
        return differenceX == differenceY
    }

    /**
     *méthode qui permet de calculer le nombre de case d'un déplacement
     * @return le nombre de case que le pion sera déplacée
     */
    fun longueur(): Int {
        val distanceX = (origine.getX() - destination.getX()).absoluteValue
        val distanceY = (origine.getY() - destination.getY()).absoluteValue
        if (estDiagonal()) return distanceX
        return distanceX + distanceY
    }


    /**
     * méthode qui permet de déterminer le sens d'un déplacement vertical
     *
     *@return true si le déplacement est positif, false sinon
     */
    fun estVerticalPositif():Boolean {
        return (estVertical() && origine.getY() - destination.getY() < 0)
    }

    /**
     * méthode qui permet de déterminer le sens d'un déplacement horizontal
     *
     * @return true si le déplacement est positif, false sinon
     */
    fun estHorizontalPositif():Boolean = (estHorizontal() && origine.getX() - destination.getX() < 0)

    /**
     * méthode qui permet de déterminer si le sens d'un déplacement diagonal est positif en X et en Y
     *
     * @return true si le déplacement est positif en X et Y, false sinon
     */
    fun estDiagonalPositifXPositifY(): Boolean {
        val differenceX = (origine.getX() - destination.getX())
        val differenceY = (origine.getY() - destination.getY())
        return differenceX < 0 && differenceY < 0 && estDiagonal()
    }

    /**
     * méthode qui permet de déterminer si le sens d'un déplacement diagonal est négatif en X et positif en Y
     *
     * @return true si le déplacement est négatif en X et positif en Y, false sinon
     */
    fun estDiagonalNegatifXPositifY(): Boolean{
        val differenceX = (origine.getX() - destination.getX())
        val differenceY = (origine.getY() - destination.getY())
        return differenceX > 0 && differenceY < 0 && estDiagonal()
    }

    /**
     *
     * méthode qui permet de déterminer si le sens d'un déplacement diagonal est positif en X et négatif en Y
     *
     * @return true si le déplacement est positif en X et négatif en Y, false sinon
     */
    fun estDiagonalPositifXNegatifY(): Boolean{
        val differenceX = (origine.getX() - destination.getX())
        val differenceY = (origine.getY() - destination.getY())
        return differenceX < 0 && differenceY > 0 && estDiagonal()
    }

    /**
     * méthode qui permet de déterminer si le sens d'un déplacement diagonal est négatif en X et négatif en Y
     *
     * @return true si le déplacement est négatif en X et négatif en Y, false sinon
     */
    fun estDiagonalNegatifXNegatifY(): Boolean{
        val differenceX = (origine.getX() - destination.getX())
        val differenceY = (origine.getY() - destination.getY())
        return differenceX > 0 && differenceY > 0 && estDiagonal()
    }

    /**
     * donne le chemin de coordonnées que constitue le déplacement
     * du point de départ vers le point d'arrivée si le déplacement demandé est vertical.
     *
     * @return une liste de coordonnées qui constitue le déplacement du point de départ vers le point d'arrivée
     * si le déplacement est vertical. Le point de départ n'est pas stocké dans la liste.
     * @throws DeplacementException est levée si le déplacement n'est pas vertical
     */
    fun getCheminVertical(): List<Coordonnee> {
        if (!estVertical())
            throw DeplacementException("le deplacement n'est pas vertical")
        var liste : List<Coordonnee> = listOf()
        val x : Int = destination.getX()
        if (origine.getY() > destination.getY()){
            for (k in origine.getY() - 1 downTo destination.getY()) liste = liste.plus(Coordonnee(x,k))
        }
        else{
            for (k in origine.getY() + 1 .. destination.getY()) liste = liste.plus(Coordonnee(x,k))
        }
        return liste
    }


    /**
     * donne le chemin de coordonnées que constitue le déplacement
     * du point de départ vers le point d'arrivée si le déplaceme{"origine Y dépasse"}nt demandé est horizontal.
     *
     * @return une liste de coordonnées qui constitue le déplacement du point de départ vers le point d'arrivée.
     * Le point de départ n'est pas stocké dans la liste.
     * si le déplacement est horizontal
     * @throws DeplacementException est levée si le déplacement n'est pas horizontal
     */
    fun getCheminHorizontal(): List<Coordonnee> {
        if (!estHorizontal())
            throw DeplacementException("le deplacement n'est pas horizontal")
        var liste : List<Coordonnee> = listOf()
        val y : Int = destination.getY()
        if (origine.getX() > destination.getX()){
            for (k in origine.getX()-1 downTo destination.getX()) liste = liste.plus(Coordonnee(k,y))
        }
        else{
            for (k in origine.getX() + 1 .. destination.getX()) liste = liste.plus(Coordonnee(k,y))
        }
        return liste
    }


    /**
     * donne le chemin de coordonnées que constitue le déplacement
     * du point de départ vers le point d'arrivée si le déplacement demandé est diagonal.
     * Le point de départ n'est pas stocké dans la liste.
     *
     * @return une liste de coordonnées qui constitue le déplacement du point de départ vers le point d'arrivée
     * si le déplacement est diagonal
     * @throws DeplacementException est levée si le déplacement n'est pas diagonal
     */
    fun getCheminDiagonal(): List<Coordonnee> {
        if (!estDiagonal()) throw DeplacementException("le deplacement n'est pas diagonale")
        var liste: List<Coordonnee> = listOf()
        val x = -(origine.getX() - destination.getX()) / (origine.getX() - destination.getX()).absoluteValue
        val y = -(origine.getY() - destination.getY()) / (origine.getY() - destination.getY()).absoluteValue
        for (k in 1..longueur()) liste = liste.plus(Coordonnee(origine.getX() + k * x, origine.getY() + k * y))
        return liste.toList()
    }


}
