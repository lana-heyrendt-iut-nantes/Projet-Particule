package projet.echecmartien.modele


import projet.echecmartien.librairie.EnumPion
import projet.echecmartien.librairie.GeneralData
import projet.echecmartien.librairie.TAILLEHORIZONTALE
import projet.echecmartien.librairie.TAILLEVERTICALE


class Plateau(){

    private var tailleHorizontale : Int = TAILLEHORIZONTALE
    private var tailleVerticale : Int = TAILLEVERTICALE
    private var cases : Array<Array<Case>> = Array(tailleHorizontale) { Array(tailleVerticale) { Case() } }
    /**
     * initialise le plateau de jeu avec les pions
     */
    fun initialiser() {
        for (i in 0 until tailleHorizontale){
            for (j in 0 until tailleVerticale){
                cases[i][j].setCoordonnee(Coordonnee(i,j))
            }
        }
        val tableau = GeneralData().tableau
        for (i in tableau.indices){
            for(j in 0 until tableau[i].size){
                if(tableau[i][j] != EnumPion.LIBRE){
                    val typePion : Pion? = when (tableau[i][j]) {
                        EnumPion.PETITPION -> PetitPion()
                        EnumPion.MOYENPION -> MoyenPion()
                        EnumPion.GRANDPION -> GrandPion()
                        else -> null
                    }
                    cases[i][j].setPion(typePion)
                }
            }
        }

    }



    /**
     * donne la taille horizontale du plateau
     * @return la taille horizontale du plateau
     */
    fun getTailleHorizontale(): Int = this.tailleHorizontale


    /**
     * donne la taille verticale du plateau
     * @return la taille verticale du plateau
     */
    fun getTailleVerticale(): Int = tailleVerticale


    /**
     * donne le tableau des cases du plateau
     * @return les cases du plateau
     */
    fun getCases(): Array<Array<Case>> = this.cases

    fun setCases(cases : Array<Array<Case>>){
        this.cases = cases
    }

    override fun toString(): String {
        var pion : Pion
        var chainePions = ""
        for(i in 0 until getTailleVerticale()){
            for (j in 0 until getTailleHorizontale()){
                if (cases[j][i].estLibre()){
                    chainePions = "$chainePions ."
                }else{
                    pion = cases[j][i].getPion()!!
                    chainePions = "$chainePions ${pion.type()}"
                }
            }
        }
        return chainePions
    }

    override fun equals(other: Any?): Boolean {
        if(other is Plateau){
            return this.toString() == other.toString()
        }
        return false
    }

    /* Fonction qui place un Pion au coordonnées données en entré */
    fun placePion(posHorizontal: Int,posVertical: Int,pion : Pion?) {
        cases[posHorizontal][posVertical].setPion(pion)
    }

    fun videPlateau() {
        for (i in 0 until this.getTailleHorizontale()) {
            for (j in 0 until this.getTailleVerticale()) {
                this.placePion(i,j,null)
            }
        }
    }

    // Vérifie si des coordonnées sont dans le plateau
    fun verifieCoordonneeValide(coord : Coordonnee) : Boolean {
        return coord.getX() in 0 until TAILLEHORIZONTALE && coord.getY() in 0 until TAILLEVERTICALE
    }

    // Fonction qui depuis d'une coordonnée renvoi une liste de toutes les coordonnées adjacentes ( +/- 1 case a la Verticale, Horizontale, Diagonale )
    fun GenCoorDonneeAdjacente(coord : Coordonnee) : MutableList<Coordonnee> {
        val listCoord : MutableList<Coordonnee> = mutableListOf()
        if (!this.verifieCoordonneeValide(coord)) {
            return listCoord
        }
        for (x in listOf<Int>(coord.getX()-1,coord.getX(),coord.getX()+1)) {
            for (y in listOf<Int>(coord.getY()-1,coord.getY(),coord.getY()+1)) {
                val c = Coordonnee(x,y)
                if (this.verifieCoordonneeValide(c) && c != coord) {
                    listCoord.add(c)
                }
            }
        }
        return listCoord
    }
}