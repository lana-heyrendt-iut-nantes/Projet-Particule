package projet.echecmartien.modele

import projet.echecmartien.librairie.DeplacementException


class MoyenPion : GrandPion() {
    override fun getScore() : Int {
        return 2
    }

    override fun getDeplacement(deplacement: Deplacement): List<Coordonnee> {
        if (deplacement.longueur() > 2) {
            throw DeplacementException("Les pions moyens ne bouge que de deux cases maximum")
        }
        return super.getDeplacement(deplacement)
    }

    override fun type(): String {
        return "Moyen"
    }

}