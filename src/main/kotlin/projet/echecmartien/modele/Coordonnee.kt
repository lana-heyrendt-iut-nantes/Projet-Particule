package projet.echecmartien.modele

class Coordonnee(private val x: Int, private val y : Int) {
    /**
     *@return la coordonnée en x
     */
    fun getX(): Int = x

    /**
     *@return la coordonnée en y
     */
    fun getY(): Int = y

    override fun toString():String = "coordonnées: x = $x; y = $y "

    override fun equals(other : Any?): Boolean {
        if (other !is Coordonnee)return false
        return this.getX() == other.getX() && this.getY() == other.getY()
    }
}