package projet.echecmartien.modele

import projet.echecmartien.librairie.DeplacementException

open class PetitPion : Pion() {

    override fun getScore() : Int {
        return 1
    }

    override fun getDeplacement(deplacement: Deplacement): List<Coordonnee> {
        if (deplacement.estDiagonal() && deplacement.longueur() == 1 ) {
            return deplacement.getCheminDiagonal()
        } else {
            throw DeplacementException("Les petits pions se déplacent uniquement en diagonale de 1(une) case")
        }
    }

    override fun type(): String {
        return "Petit"
    }


}