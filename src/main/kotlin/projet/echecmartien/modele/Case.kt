package projet.echecmartien.modele


import javafx.scene.layout.*
import projet.echecmartien.librairie.*


open class Case{
    var estLibre : Boolean
    private var pion : Pion?
    private var joueur : Joueur?
    var coodonnees : Coordonnee?

    init {
        this.estLibre = true
        this.pion = null
        this.joueur = null
        this.coodonnees = Coordonnee(-1,-1)
    }

    /**
     * teste si une case contient un pion ou non
     * @return true si la case ne contient pas un pion, false sinon.
     */
    fun estLibre(): Boolean {
       return estLibre
    }

    /** getter
     * @return le joueur associé à la case
     */
    fun getJoueur():Joueur? {
        return this.joueur
    }

    /** setter
     * @param joueur qui est associé à la casevueJeu.jeu.listeDeplacements(case.getCoordonnee().getX(), case.getCoordonnee().getY())
     */
    fun setJoueur(joueur: Joueur?) {
        this.joueur = joueur
    }
    fun setCoordonnee(coo: Coordonnee){
        this.coodonnees = coo
    }
    fun getCoordonnee():Coordonnee{
        return this.coodonnees!!
    }

    /** getter
     * @return le pion associé à la case
     */
    fun getPion():Pion? {
        return this.pion
    }

    /** setter
     * @param pion qui est associé à la case
     */
    override fun equals(other: Any?): Boolean {
        if (other is Case){
            if (this.estLibre() == other.estLibre() && this.pion == other.pion && this.joueur == other.joueur && this.coodonnees!!.getX() == other.coodonnees!!.getX() && this.coodonnees!!.getY() == other.coodonnees!!.getY()){
                return true
            }
        }
        return false
    }
    fun setPion(pion: Pion?) {
        this.pion = pion
        this.estLibre = pion == null
    }

}
