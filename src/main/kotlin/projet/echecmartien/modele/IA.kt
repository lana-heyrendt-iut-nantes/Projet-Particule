package projet.echecmartien.modele

import projet.echecmartien.librairie.TAILLEHORIZONTALE
import projet.echecmartien.librairie.TAILLEVERTICALE
import projet.echecmartien.vue.VueJeu
import java.lang.Integer.max
import java.lang.Integer.min

class IA(vueJeu: VueJeu) {
    var jeu = vueJeu.jeu

    private fun calculerScore(partie: Jeu, tourIA: Boolean): Int{
        var score = 0
        if (tourIA){
            score = partie.getJoueurCourant()!!.calculerScore()
            partie.changeJoueurCourant()
            score -= partie.getJoueurCourant()!!.calculerScore()
            partie.changeJoueurCourant()
        }else {
            partie.changeJoueurCourant()
            score = partie.getJoueurCourant()!!.calculerScore()
            partie.changeJoueurCourant()
            score -= partie.getJoueurCourant()!!.calculerScore()
        }
        return score
    }

    fun copy(jeuACopier:Jeu):Jeu{
        val nouveauJeu = Jeu()
        nouveauJeu.initialiserPartie(Joueur(jeuACopier.getNomsJoueurs().first),Joueur(jeuACopier.getNomsJoueurs().second),jeuACopier.getNbCoupSanPriseMax())
        nouveauJeu.initialiserJoueur(Joueur(jeuACopier.getNomsJoueurs().first),Joueur(jeuACopier.getNomsJoueurs().second))
        nouveauJeu.setNbCoupSanPrise(jeuACopier.getNbCoupSanPrise())

        if (nouveauJeu.getJoueurCourant()!!.getPseudo() != jeuACopier.getJoueurCourant()!!.getPseudo()){
            nouveauJeu.changeJoueurCourant()
        }

        nouveauJeu.setCoordOrigineDeplacement(jeuACopier.getCoordOrigineDeplacement()!!)
        nouveauJeu.setCoordDestinationDeplacement(jeuACopier.getCoordDestinationDeplacement()!!)

        //ajout des pions capturés
        for(pion in jeuACopier.getJoueurCourant()!!.getPionsCaptures()){
            nouveauJeu.getJoueurCourant()!!.ajouterPionCaptures(pion)
        }
        jeuACopier.changeJoueurCourant()
        nouveauJeu.changeJoueurCourant()
        for(pion in jeuACopier.getJoueurCourant()!!.getPionsCaptures()){
            nouveauJeu.getJoueurCourant()!!.ajouterPionCaptures(pion)
        }
        jeuACopier.changeJoueurCourant()
        nouveauJeu.changeJoueurCourant()


        //création du plateau
        val copiePlateau = Plateau()
        for (i in 0 until TAILLEHORIZONTALE){
            for (j in 0 until TAILLEVERTICALE){
                copiePlateau.getCases()[i][j] = Case()
                if(jeuACopier.getPlateau().getCases()[i][j].getPion() != null ){
                    if(jeuACopier.getPlateau().getCases()[i][j].getPion()!!.type() == "Grand" ){
                        copiePlateau.getCases()[i][j].setPion(GrandPion())
                    }
                    if(jeuACopier.getPlateau().getCases()[i][j].getPion()!!.type() == "Moyen" ){
                        copiePlateau.getCases()[i][j].setPion(MoyenPion())
                    }
                    if(jeuACopier.getPlateau().getCases()[i][j].getPion()!!.type() == "Petit" ){
                        copiePlateau.getCases()[i][j].setPion(PetitPion())
                    }
                }
            }
        }
        nouveauJeu.setPlateau(copiePlateau)

        return nouveauJeu
    }

    fun miniMax(partie: Jeu, nbCoupsDAvance: Int,tourIA: Boolean, alpha: Int,beta: Int): Pair<Int,MutableList<Int>>{
        var alpha2 = alpha
        var beta2 = beta

        val listeCoups = mutableListOf<Int>()
        var listeCoupsPossibles: MutableList<Int>
        var partieParallele : Jeu
        var deplacementOrigineX : Int = -1
        var deplacementOrigineY : Int = -1
        var deplacementDestX : Int = -1
        var deplacementDestY : Int = -1
        var meilleurcoup: Pair<Int, MutableList<Int>>

        if(nbCoupsDAvance == -1 || partie.arretPartie()){
            val score = calculerScore(partie, tourIA)
            return Pair(score,listeCoups)
        }
        if (tourIA){
            var score = Int.MIN_VALUE
            meilleurcoup = Pair(Int.MIN_VALUE,listeCoups)
            for (i in 0 until TAILLEHORIZONTALE){
                for(j in 0 until TAILLEVERTICALE){
                    if(partie.deplacementPossible(i,j)){
                        listeCoupsPossibles = partie.listeDeplacements(i,j, partie.getJoueurCourant())
                        for (k in 0 until  listeCoupsPossibles.size/2){
                            partieParallele = copy(partie)
                            partieParallele.deplacer(i,j,listeCoupsPossibles[2*k],listeCoupsPossibles[(2*k)+1])
                            val coupSuivant = miniMax(partieParallele,nbCoupsDAvance-1,false,alpha2,beta2)
                            if (coupSuivant.first > meilleurcoup.first){
                                meilleurcoup = coupSuivant
                                deplacementOrigineX = i
                                deplacementOrigineY = j
                                deplacementDestX = listeCoupsPossibles[2*k]
                                deplacementDestY =  listeCoupsPossibles[(2*k)+1]
                            }
                            score = max(coupSuivant.first,score)
                            alpha2 = max(alpha2,score)
                            if(beta2 <= alpha2) break
                        }
                    }
                }
            }
        }
        else{
            var score = Int.MAX_VALUE
            meilleurcoup = Pair(Int.MAX_VALUE,listeCoups)
            for (i in 0 until TAILLEHORIZONTALE){
                for(j in 0 until TAILLEVERTICALE){
                    if(partie.deplacementPossible(i,j)){
                        listeCoupsPossibles = partie.listeDeplacements(i,j, partie.getJoueurCourant())
                        for (k in 0 until  listeCoupsPossibles.size/2){
                            partieParallele = copy(partie)
                            partieParallele.deplacer(i,j,listeCoupsPossibles[2*k],listeCoupsPossibles[(2*k)+1])
                            val coupSuivant = miniMax(partieParallele,nbCoupsDAvance-1,true, alpha2,beta2)
                            if (coupSuivant.first < meilleurcoup.first){
                                meilleurcoup = coupSuivant
                                deplacementOrigineX = i
                                deplacementOrigineY = j
                                deplacementDestX = listeCoupsPossibles[2*k]
                                deplacementDestY =  listeCoupsPossibles[(2*k)+1]
                            score = min(coupSuivant.first,score)
                            beta2 = min(alpha2,score)
                            if(beta2 <= alpha2) break
                            }
                        }
                    }
                }
            }
        }

        meilleurcoup.second.add(deplacementOrigineX)
        meilleurcoup.second.add(deplacementOrigineY)
        meilleurcoup.second.add(deplacementDestX)
        meilleurcoup.second.add(deplacementDestY)
        return meilleurcoup
    }
}