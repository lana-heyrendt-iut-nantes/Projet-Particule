package projet.echecmartien.modele


open class GrandPion : Pion() {
    override fun getScore(): Int {
        return 3
    }

    override fun getDeplacement(deplacement: Deplacement): List<Coordonnee> {
        when {
            deplacement.estHorizontal() -> return deplacement.getCheminHorizontal()
            deplacement.estVertical() -> return deplacement.getCheminVertical()
        }
        return deplacement.getCheminDiagonal()    }

    override fun type(): String {
        return "Grand"
    }
}