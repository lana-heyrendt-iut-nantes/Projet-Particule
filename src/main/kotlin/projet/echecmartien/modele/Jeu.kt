package projet.echecmartien.modele

import projet.echecmartien.librairie.*


class Jeu() : InterfaceJeu{
    private var nombreCoupsSansPriseMax : Int = 0
    private var nombreCoupsSansPrise : Int = 0
    private val plateau : Plateau = Plateau()
    private var joueurs : ArrayList<Joueur> = arrayListOf(Joueur("Joueur 1"),Joueur("Joueur 2"))
    private var joueurCourant : Joueur? = null
    private var coordOrigine : Coordonnee? = null
    private var coordDest : Coordonnee? = null
    private var ia : Boolean = false
    /**
     * getter
     * @return la coordonnée origine du déplacement
     */
    fun getCoordOrigineDeplacement(): Coordonnee? = coordOrigine
    /**
     * getter
     * @return la coordonnée destination du déplacement
     */
    fun getCoordDestinationDeplacement(): Coordonnee? = coordDest


    /**
     * setter
     * @param origine la coordonnée origine du déplacement
     */
    fun setCoordOrigineDeplacement(origine: Coordonnee){
       coordOrigine = origine
    }


    /**
     * setter
     * @param destination la coordonnée destination du déplacement
     */
    fun setCoordDestinationDeplacement(destination: Coordonnee){
        coordDest = destination
    }


    /** retourne le joueur courant
     * @return le joueur courant
     */
    fun getJoueurCourant(): Joueur? = joueurCourant

    fun getNomsJoueurs(): Pair<String,String> = Pair(joueurs[0].getPseudo(),joueurs[1].getPseudo())

/**
     * affectation des joueurs aux cases
     * @param joueur1 premier joueur
     * @paral joueur2 second joueur
     */
    fun initialiserJoueur(joueur1: Joueur, joueur2: Joueur) {
        for (colonne in plateau.getCases()) {
            for (case in colonne.indices) {
                if (case < colonne.size/2) {
                    colonne[case].setJoueur(joueur1)
                }else{
                    colonne[case].setJoueur(joueur2)
                }

            }
        }

    }

    fun setIA(){
        ia = true
    }
    /**
     * permet de savoir si la partie est finie ou non
     * @return true si la partie est finie, false sinon
     */
    fun iaActivee(): Boolean{
        return ia
    }
    fun arretPartie(): Boolean {
        if (nombreCoupsSansPrise >= nombreCoupsSansPriseMax) return true
        var unSeulPionRestant = false
        for (colonne in plateau.getCases()){
            for (case in colonne){
                if (!case.estLibre) {
                    if (unSeulPionRestant) {
                        return false
                    }
                    unSeulPionRestant = true
                }
            }

        }
        return true
    }


    /**
     * modifie le joueur courant
     *
     */
    fun changeJoueurCourant() {
        joueurCourant = if (getJoueurCourant() == null || getJoueurCourant() == this.joueurs[1]) {
            this.joueurs[0]
        }
        else{
            this.joueurs[1]
        }
    }

    override fun initialiserPartie(joueur1: Joueur, joueur2: Joueur, nombreCoupsSansPriseMax: Int) {
        joueurs = arrayListOf(joueur1,joueur2)
        plateau.initialiser()
        initialiserJoueur(joueur1,joueur2)
        changeJoueurCourant()
        this.nombreCoupsSansPriseMax = nombreCoupsSansPriseMax
    }


    // Fonction qui renvoi à quels joueurs appartiennes des coordonnées du plateau
    private fun coteDuPlateau(coord: Coordonnee) : Joueur? {
        // Si les coordinates sont invalides retourne null
        return when {
            !this.plateau.verifieCoordonneeValide(coord) -> null
            coord.getY() < TAILLEVERTICALE / 2 -> this.joueurs[0]
            else -> this.joueurs[1]
        }
    }

    override fun deplacementPossible(coordOrigineX: Int, coordOrigineY: Int): Boolean {
        val coordPion = Coordonnee(coordOrigineX,coordOrigineY)
        // Vérifie si les coordonnées sont cohérentes
        when {
            // Si les coordonnées n'existe pas sur le plateau
            !this.plateau.verifieCoordonneeValide(coordPion) -> {return false}
            // Si il n'y a pas de pion sur la case
            this.plateau.getCases()[coordOrigineX][coordOrigineY].estLibre() -> { return false }
            // Si le pion n'appartient pas au joueur courant
            this.joueurCourant != this.coteDuPlateau(coordPion) -> { return false }
        }
        // Génére une liste des coordonnées des cases adjacentes et regarde si un déplacement est possible pour chaque coordonnée
        val listAdj : MutableList<Coordonnee> = this.plateau.GenCoorDonneeAdjacente(coordPion)
        for (crd in listAdj) {
            when {
                // Si le pion est un petit pion, on ignore les déplacements horizontaux et verticaux
                this.plateau.getCases()[coordOrigineX][coordOrigineY].getPion() is PetitPion && !Deplacement(coordPion, crd).estDiagonal() -> {}
                // Si le pion à changer de zone au tour du joueur précédent, on ignore les déplacements ou il change une nouvelle fois de zone
                this.getCoordDestinationDeplacement() == coordPion && this.joueurCourant != this.coteDuPlateau(crd) -> {}
                // Si le pion peut se déplacer sur une case sans pion
                this.plateau.getCases()[crd.getX()][crd.getY()].getPion() !is Pion -> return true
                // Si le pion peut se déplacer vers une des cases de la zone adverse
                this.joueurCourant != this.coteDuPlateau(crd) -> return true
            }
        }
        // Si aucune des cases dans la liste n'est une case de déplacement valide, le pion est dans l'incapacité de se déplacer
        return false
    }

    override fun deplacementPossible(
        coordOrigineX: Int, coordOrigineY: Int, // Coordonnées d'origine'
        coordDestinationX: Int, coordDestinationY: Int, // Coordonnées d'arriver
        joueur: Joueur?
    ): Boolean {
        val coordOrg = Coordonnee(coordOrigineX,coordOrigineY)
        val coordDest = Coordonnee(coordDestinationX,coordDestinationY)
        when {
            // Si les coordonées ne sont pas valide renvoit false
            !this.plateau.verifieCoordonneeValide(coordOrg) -> return false
            !this.plateau.verifieCoordonneeValide(coordDest) -> return false
            // Si le pion n'existe pas ou n'appartient pas au joueur passer en parametre renvoit false
            coteDuPlateau(coordOrg) != joueur -> return false
            this.plateau.getCases()[coordOrigineX][coordOrigineY].estLibre() -> return false
            // Si le pion à changer de zone au tour précédent il ne peut pas le faire maintenant
            this.getCoordDestinationDeplacement() == coordOrg && joueur != coteDuPlateau(coordDest) -> return false
        }
        // Si les cases entre l'origine et la destination ne sont pas libre revoit false
        val pion : Pion = this.plateau.getCases()[coordOrigineX][coordOrigineY].getPion()!!
        // Vérification que le déplacement est possible
        try { pion.getDeplacement(Deplacement(coordOrg,coordDest)) } catch (e: DeplacementException) { return false }
        for (crd in pion.getDeplacement(Deplacement(coordOrg,coordDest)).dropLast(1)) {
            if (!this.plateau.getCases()[crd.getX()][crd.getY()].estLibre()) {
                return false
            }
        }
        return when {
            this.plateau.getCases()[coordDestinationX][coordDestinationY].estLibre() -> true // Si la case d'arrivée est vide
            joueur != coteDuPlateau(coordDest) -> true // Si la case d'arrivée contient un
            else -> false
        }
    }

    override fun deplacer(coordOrigineX: Int, coordOrigineY: Int, coordDestinationX: Int, coordDestinationY: Int) {
        if(deplacementPossible(coordOrigineX,coordOrigineY,coordDestinationX,coordDestinationY,joueurCourant)){
            if(plateau.getCases()[coordDestinationX][coordDestinationY].estLibre()){
                nombreCoupsSansPrise ++
            }else{
                nombreCoupsSansPrise = 0
                joueurCourant!!.ajouterPionCaptures(plateau.getCases()[coordDestinationX][coordDestinationY].getPion()!!)
                plateau.getCases()[coordDestinationX][coordDestinationY].setPion(null)
            }
            plateau.getCases()[coordDestinationX][coordDestinationY].setPion(plateau.getCases()[coordOrigineX][coordOrigineY].getPion())
            plateau.getCases()[coordOrigineX][coordOrigineY].setPion(null)
            setCoordOrigineDeplacement(Coordonnee(coordOrigineX,coordOrigineY))
            setCoordDestinationDeplacement(Coordonnee(coordDestinationX, coordDestinationY))
            changeJoueurCourant()
        }
    }

    override fun joueurVainqueur(): Joueur? {
        if (joueurs[0].calculerScore() > joueurs[1].calculerScore()){
            return joueurs[0]
        }
        if (joueurs[0].calculerScore() < joueurs[1].calculerScore()){
            return joueurs[1]
        }
        return null
    }

    fun setPlateau(plateau: Plateau) {
        this.plateau.setCases(plateau.getCases())
    }
    fun listeDeplacements(x: Int, y: Int, joueur: Joueur?) :MutableList<Int>{
        val lst = mutableListOf<Int>()
        for (i in 0 until plateau.getTailleHorizontale()){
            for (j in 0 until plateau.getTailleVerticale()){
                if (deplacementPossible(x,y,i,j,joueur)){
                    lst.add(i)
                    lst.add(j)
                }
            }
        }
        return lst
    }
    fun getPlateau() : Plateau {
        return this.plateau
    }
    fun getNbCoupSanPriseMax(): Int{
        return nombreCoupsSansPriseMax
    }
    fun getNbCoupSanPrise(): Int{
        return nombreCoupsSansPrise
    }
    fun setNbCoupSanPrise( nb: Int){
        nombreCoupsSansPrise = nb
    }

    fun getCoordDest(): Coordonnee? = coordDest
    fun getCoordOrg(): Coordonnee? = coordOrigine
    fun load(cheminAcces : String){
        var step = 0
        var nomJoueur1 = ""
        var nomJoueur2 = ""
        var x =0
        var y = 0
        var xOrg : Int = -1
        var xDest : Int = -1
        var nombreCoupMaxLoad : Int = NOMBREDECOUPMAX
        val cases : Array<Array<Case>> = Array(TAILLEHORIZONTALE) { Array(TAILLEVERTICALE) { Case() } }

        for (k in cheminAcces){
            if (x == TAILLEHORIZONTALE) break
            if (k == ';'){
                step +=1
                continue
            }
            when(step){
                0 -> nomJoueur1 += k
                1 -> nomJoueur2 += k
                2 -> if(k == 'o') setIA()
                3 -> if(k =='1') changeJoueurCourant()
                4 -> { if (k.isDigit() && k.digitToInt()>-1 ){
                        nombreCoupsSansPrise = k.digitToInt()
                }
                        else{
                            println("$step , $k")
                throw(LoadingException())
                        }
            }
                5 -> { if (k.isDigit() && k.digitToInt()>-1 && k.digitToInt()<5){
                    xOrg = k.digitToInt()
                }
                else{
                    println("$step , $k")
                    throw(LoadingException())
                }
                }
                6 -> { if (k.isDigit()  && k.digitToInt()>-1 && k.digitToInt()<8){
                    coordOrigine = Coordonnee(xOrg,k.digitToInt())
                }
                else{
                    println("$step , $k")
                    if (k !='n') throw(LoadingException())

                }
                }
                7 -> { if (k.isDigit() && k.digitToInt()>-1 && k.digitToInt()<5){
                    xDest = k.digitToInt()
                }
                else{
                    println("$step , $k")
                    throw(LoadingException())
                }
                }
                8 -> { if (k.isDigit()  && k.digitToInt()>-1 && k.digitToInt()<8){
                    coordOrigine = Coordonnee(xDest,k.digitToInt())
                }
                else{
                    println("$step , $k")
                    if (k != 'n') throw(LoadingException())
                }
                }
                9 ->{ if (k.isDigit()  && k.digitToInt()>-1){
                    nombreCoupMaxLoad = k.digitToInt()
                }
                else{
                    println("$step , $k")
                    throw(LoadingException())
                }
                }
                else -> {
                    when (k){
                        'P' -> cases[x][y].setPion(PetitPion())
                        'M' -> cases[x][y].setPion(MoyenPion())
                        'G' -> cases[x][y].setPion(GrandPion())
                        'X' -> cases[x][y].setPion(null)
                        else -> throw(LoadingException())
                    }

                    y +=1
                    if (y == TAILLEVERTICALE ) {
                        y = 0
                        x += 1
                    }
                }
            }

        }
        if (x != TAILLEHORIZONTALE  || y != 0){
            throw(LoadingException())
        }
        initialiserPartie(Joueur(nomJoueur1), Joueur(nomJoueur2), nombreCoupMaxLoad)

        val plateau = Plateau()
        for (i in 0 until TAILLEHORIZONTALE){
            for (j in 0 until TAILLEVERTICALE){
                if (j < TAILLEVERTICALE/2) {
                    cases[i][j].setJoueur(joueurs[0])
                }else{
                    cases[i][j].setJoueur(joueurs[1])
                }
                cases[i][j].setCoordonnee(Coordonnee(i,j))
            }
        }
        plateau.setCases(cases)
        this.setPlateau(plateau)
    }
}