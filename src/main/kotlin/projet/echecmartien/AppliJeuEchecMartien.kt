package projet.echecmartien

import javafx.application.Application
import javafx.event.ActionEvent
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.input.MouseEvent
import javafx.stage.Stage
import javafx.stage.StageStyle
import javafx.stage.WindowEvent
import org.controlsfx.control.action.Action
import projet.echecmartien.controleur.*
import projet.echecmartien.librairie.BACKGROUND
import projet.echecmartien.librairie.HAUTEURFENETRE
import projet.echecmartien.librairie.LARGEURFENETRE
import projet.echecmartien.modele.Case
import projet.echecmartien.modele.Jeu
import projet.echecmartien.vue.*

class AppliJeuEchecMartien: Application() {
    var derniereCase = Case()
    var jeu = Jeu()
    override fun start(primaryStage: Stage) {

        //Les VUES
        val vueJeu = VueJeu(this)
        val menu = Menu()
        val regles = Regles()
        val pseudoJoueur = PseudoJoueur()
        val menuPartie = MenuGenerique()
        val chargerPartie = ChargerPartie()
        val accessibilite = Accessibilite()
        val victoire = Victoire("test")
        //FOND D'ECRANCS DES VUES
        menu.style = """
            -fx-background-image: url('file:$BACKGROUND');
            -fx-background-size: cover;
        """.trimMargin()
        pseudoJoueur.style = """
            -fx-background-image: url('file:$BACKGROUND');
            -fx-background-size: cover;
        """.trimMargin()
        menuPartie.style = """
            -fx-background-image: url('file:$BACKGROUND');
            -fx-background-size: cover;
        """.trimMargin()
        accessibilite.style = """
            -fx-background-image: url('file:$BACKGROUND');
            -fx-background-size: cover;
        """.trimMargin()

        //Les SCENES
        val sceneMenu = createScene(menu)
        val sceneRegles = createScene(regles)
        val scenePseudoJoueur = createScene(pseudoJoueur)
        val sceneVueJeu = createScene(vueJeu)
        val sceneMenuPartie = createScene(menuPartie)
        val sceneChargerPartie = createScene(chargerPartie)
        val sceneAccessibilite = createScene(accessibilite)
        
        //STAGE
        primaryStage.title = "Echecs Martiens"
        primaryStage.scene = sceneMenu
        primaryStage.isResizable = true

        //CONTROLEURS
        menu.regle.addEventHandler(ActionEvent.ACTION, ControlleurChangePage(regles,primaryStage, sceneRegles))
        menu.nouvellePartie.addEventHandler(ActionEvent.ACTION,ControlleurChangePage(menuPartie,primaryStage,sceneMenuPartie))
        menu.accessibilite.addEventHandler(ActionEvent.ACTION,ControlleurChangePage(accessibilite,primaryStage,sceneAccessibilite))
        menu.quitter.addEventHandler(ActionEvent.ACTION,ControlleurExitApplication(primaryStage, menu))
        regles.retour.addEventHandler(ActionEvent.ACTION, ControlleurChangePage(menu,primaryStage, sceneMenu))
        accessibilite.bouttonRetour.addEventHandler(ActionEvent.ACTION, ControlleurChangePage(menu,primaryStage, sceneMenu))
        accessibilite.boxDarkMode.addEventHandler(ActionEvent.ACTION, ControlleurAccessibiliteDarkMode(accessibilite))
        accessibilite.boxDaltonien.addEventHandler(ActionEvent.ACTION, ControlleurAccessibiliteDaltonien(accessibilite))
        accessibilite.choixPolice.addEventHandler(ActionEvent.ACTION, ControlleurAccessibilitePolice(accessibilite))
        accessibilite.bouttonCharger.addEventHandler(ActionEvent.ACTION, ControlleurAccessibiliteBackground(accessibilite))
        pseudoJoueur.bouttonJouer.addEventHandler(ActionEvent.ACTION,ControlleurInitJeu(pseudoJoueur,jeu,vueJeu,primaryStage,sceneVueJeu, this,victoire))
        pseudoJoueur.bouttonRetour.addEventHandler(ActionEvent.ACTION, ControlleurChangePage(menuPartie,primaryStage, sceneMenuPartie))
        val ctrlMenuGenerique = ControlleurMenuGenerique(menuPartie,primaryStage,sceneMenu, sceneChargerPartie, scenePseudoJoueur, sceneVueJeu,jeu,vueJeu, pseudoJoueur, chargerPartie, this, victoire)
        menuPartie.btn1.addEventHandler(ActionEvent.ACTION,ctrlMenuGenerique)
        menuPartie.btn2.addEventHandler(ActionEvent.ACTION,ctrlMenuGenerique)
        menuPartie.btn3.addEventHandler(ActionEvent.ACTION,ctrlMenuGenerique)
        chargerPartie.bouttonRetour.addEventHandler(ActionEvent.ACTION,ControlleurChangePage(menuPartie,primaryStage,sceneMenuPartie))

        chargerPartie.bouttonRetour.addEventHandler(ActionEvent.ACTION,ControlleurChangePage(menuPartie,primaryStage,sceneMenuPartie))
        chargerPartie.bouttonValider.addEventHandler(ActionEvent.ACTION, ControlleurLoadGame(chargerPartie,jeu,primaryStage,sceneVueJeu,vueJeu))
        victoire.boutonRetour.addEventHandler(ActionEvent.ACTION, ControlleurChangePage(menu,primaryStage, sceneMenu, victoire))
        vueJeu.quitter.addEventHandler(ActionEvent.ACTION, ControlleurExitGame(primaryStage,jeu,sceneMenu))
        for (i in vueJeu.plateau.getVueCases().indices){
            for (j in 0 until vueJeu.plateau.getVueCases()[i].size){
                vueJeu.plateau.getVueCases()[i][j].addEventHandler(MouseEvent.MOUSE_CLICKED,ControlleurCase(jeu.getPlateau().getCases()[i][j],vueJeu,primaryStage,sceneVueJeu,this,victoire ))
            }
        }
        primaryStage.isResizable = false

        primaryStage.show()


    }

}

fun createScene(vue: Parent) = Scene(vue,LARGEURFENETRE, HAUTEURFENETRE)
fun main(){
    Application.launch(AppliJeuEchecMartien::class.java)
}



