package projet.echecmartien.controleur

import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.scene.Scene
import javafx.scene.image.Image
import javafx.scene.layout.Background
import javafx.scene.layout.BackgroundFill
import javafx.scene.layout.BackgroundImage
import javafx.scene.layout.CornerRadii
import javafx.scene.paint.Paint
import javafx.stage.Stage
import projet.echecmartien.librairie.*
import projet.echecmartien.vue.Accessibilite

class ControlleurAccessibilitePolice (vue : Accessibilite) : EventHandler<ActionEvent> {

    var vue : Accessibilite

    init{
        this.vue = vue
    }

    override fun handle(event: ActionEvent?) {

        if (vue.choixPolice.value != null) {
            TAILLEPOLICE = vue.choixPolice.value.toInt()
        }

        vue.update()
    }
}