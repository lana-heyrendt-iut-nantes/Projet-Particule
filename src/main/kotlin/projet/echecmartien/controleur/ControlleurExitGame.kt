package projet.echecmartien.controleur

import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.stage.DirectoryChooser
import javafx.stage.Stage
import projet.echecmartien.librairie.NOMBRECOUPSDAVANCE
import projet.echecmartien.librairie.NOMBREDECOUPMAX
import projet.echecmartien.modele.GrandPion
import projet.echecmartien.modele.Jeu
import projet.echecmartien.modele.MoyenPion
import projet.echecmartien.modele.PetitPion
import projet.echecmartien.vue.Menu
import projet.echecmartien.vue.SauvegarderPartie
import java.io.File
import java.lang.reflect.Executable

class ControlleurExitGame(val stage: Stage, val jeu: Jeu, val menu: Scene): EventHandler<ActionEvent> {
    private val sauvegarderPartie = SauvegarderPartie()
    override fun handle(p0: ActionEvent?) {
        sauvegarderPartie.show()
        sauvegarderPartie.boutonSauvegarder.setOnAction {
            sauvegarderPartie.boutonSauvegarder.isDisable = true
            sauvegarderPartie.boutonQuitter.isDisable = true
            sauvegarderPartie.boutonRetour.isDisable = true
            val directoryChooser = DirectoryChooser()
            directoryChooser.title = "Choisie un dossier pour sauvegarder la partie"
            var dialog : String? = null
            while (dialog == null)
                try {dialog = directoryChooser.showDialog(null).toString()}
                catch(_: java.lang.Exception){}
            val file = File(dialog, "Test.echecsmartien")
            file.delete()
            file.createNewFile()
            var text = "${jeu.getNomsJoueurs().first};${jeu.getNomsJoueurs().second};${
                when (jeu.iaActivee()) {
                    true -> 'o'
                    false -> 'n'
                }
            };${
                when (jeu.getJoueurCourant()?.getPseudo()) {
                    jeu.getNomsJoueurs().second -> '1'
                    else -> 0
                }
            };${jeu.getNbCoupSanPrise()};${
                 when (jeu.getCoordOrg()?.getX()) {
                        null -> '0'
                        else -> jeu.getCoordOrg()?.getX().toString()
                    }
                };${
                    when (jeu.getCoordOrg()?.getY()) {
                        null -> 'n'
                        else -> jeu.getCoordOrg()?.getY().toString()
                    }
                };${
                    when (jeu.getCoordDest()?.getX()) {
                        null -> '0'
                        else -> jeu.getCoordDest()?.getX().toString()
                    }
                };${
                    when (jeu.getCoordDest()?.getY()) {
                        null -> 'n'
                        else -> jeu.getCoordDest()?.getY().toString()
                    }
                };$NOMBREDECOUPMAX;"

                for (i in jeu.getPlateau().getCases()) {
                    for (j in i) {
                        text += when (j.getPion()) {
                            is PetitPion -> 'P'
                            is MoyenPion -> 'M'
                            is GrandPion -> 'G'
                            else -> 'X'
                        }
                    }
                }
            file.appendText(text)
            sauvegarderPartie.close()
            stage.scene = menu
        }
        sauvegarderPartie.boutonQuitter.setOnAction {
            sauvegarderPartie.close()
            stage.scene = menu
        }
        sauvegarderPartie.boutonRetour.setOnAction {
            sauvegarderPartie.close()
        }

    }
}