package projet.echecmartien.controleur

import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.stage.Stage
import javafx.stage.WindowEvent
import projet.echecmartien.vue.Victoire
import projet.echecmartien.vue.Vue

class ControlleurChangePage(var vue : Vue, var stage : Stage, var scene: Scene,private val victoire: Victoire? = null): EventHandler<ActionEvent> {
    override fun handle(p0: ActionEvent?) {
        stage.scene = scene
        stage.addEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST,ControleurCloseRequest())
        victoire?.close()
        vue.update()

    }

}