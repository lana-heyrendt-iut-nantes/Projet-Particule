package projet.echecmartien.controleur

import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.stage.Stage
import projet.echecmartien.librairie.COULEURGRILLE
import projet.echecmartien.librairie.LoadingException
import projet.echecmartien.modele.Jeu
import projet.echecmartien.vue.ChargerPartie
import projet.echecmartien.vue.VueJeu
import java.io.File
import java.io.FileNotFoundException

class ControlleurLoadGame(val vue: ChargerPartie, val jeu: Jeu, val stage: Stage, val scene: Scene, val vueJeu: VueJeu) : EventHandler<ActionEvent> {
    override fun handle(p0: ActionEvent?) {
        try{
            val text = File(vue.input.text).readText()
            jeu.load(text)}
        catch (e : LoadingException){
            vue.label.text = "format de fichier invalide"
            return
        }
        catch(e : FileNotFoundException){
            vue.label.text = "chemin d'accès invalide"
            return
        }
        for (i in 0 until vueJeu.jeu.getPlateau().getTailleHorizontale()){
            for (j in 0 until vueJeu.jeu.getPlateau().getTailleVerticale()){
                vueJeu.plateau.getVueCases()[i][j].style = """
                    -fx-background-color: $COULEURGRILLE
                """.trimIndent()
            }
        }
        vue.label.text = "Veuillez saisir le chemin d'accès de la partie à charger: "
        vueJeu.update()
        stage.scene = scene

    }
}