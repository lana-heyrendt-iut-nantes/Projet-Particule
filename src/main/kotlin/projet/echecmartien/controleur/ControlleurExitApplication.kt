package projet.echecmartien.controleur

import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.stage.Stage
import projet.echecmartien.vue.Confirmation
import projet.echecmartien.vue.Menu

class ControlleurExitApplication(val stage: Stage, private val menu: Menu) :  EventHandler<ActionEvent>{
    private val confirmation: Confirmation = Confirmation()
    override fun handle(p0: ActionEvent?) {
        confirmation.show()
        menu.quitter.isDisable = true
        menu.regle.isDisable = true
        menu.nouvellePartie.isDisable = true
        menu.accessibilite.isDisable = true
        confirmation.boutonValider.setOnAction {
            stage.close()
            confirmation.close()
        }
        confirmation.boutonRetour.setOnAction {
            confirmation.close()
            menu.quitter.isDisable = false
            menu.regle.isDisable = false
            menu.nouvellePartie.isDisable = false
            menu.accessibilite.isDisable = false
        }
    }

}