package projet.echecmartien.controleur

import javafx.application.Platform
import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.stage.Stage
import javafx.stage.WindowEvent
import projet.echecmartien.vue.Victoire
import projet.echecmartien.vue.Vue

class ControleurCloseRequest: EventHandler<WindowEvent> {
    override fun handle(p0: WindowEvent?) {
        p0?.consume()
    }
}