package projet.echecmartien.controleur

import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.stage.FileChooser
import projet.echecmartien.vue.ChargerPartie

class ControlleurSelectFile(val vue: ChargerPartie): EventHandler<ActionEvent> {
    override fun handle(p0: ActionEvent?) {
        val input = FileChooser()
        input.title = "Veuillez saisir le chemin d'accès de la partie à charger: "
        val file = input.showOpenDialog(null)
        vue.input.text = file.path.toString()
    }
}