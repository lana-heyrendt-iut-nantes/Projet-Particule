package projet.echecmartien.controleur

import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.scene.input.MouseEvent
import javafx.stage.Stage
import projet.echecmartien.AppliJeuEchecMartien
import projet.echecmartien.librairie.*
import projet.echecmartien.modele.*
import projet.echecmartien.vue.Victoire
import projet.echecmartien.vue.VueJeu
import projet.echecmartien.vue.VuePlateau

class ControlleurCase(private val case: Case, private val vueJeu: VueJeu, var stage : Stage, var scene: Scene, private var appli: AppliJeuEchecMartien, private val victoire: Victoire): EventHandler<MouseEvent> {
    override fun handle(p0: MouseEvent?) {
        //COUPS POSSIBLES
        for (i in 0 until vueJeu.jeu.getPlateau().getTailleHorizontale()){
            for (j in 0 until vueJeu.jeu.getPlateau().getTailleVerticale()){
                if (j < TAILLEVERTICALE/2) {
                    vueJeu.plateau.getVueCases()[i][j].style = """
                    -fx-background-color: $COULEURGRILLE
                """.trimIndent()
                }
                else{
                    vueJeu.plateau.getVueCases()[i][j].style = """
                    -fx-background-color: $COULEURGRILLE2
                """.trimIndent()
                }
            }
        }
        val lstCoupsPossible = vueJeu.jeu.listeDeplacements(case.getCoordonnee().getX(), case.getCoordonnee().getY(),vueJeu.jeu.getJoueurCourant())
        for(i in 0 until lstCoupsPossible.size/2){

            if(vueJeu.jeu.getJoueurCourant()!!.getPseudo() == vueJeu.jeu.getNomsJoueurs().first){
                vueJeu.plateau.getVueCases()[lstCoupsPossible[2*i]][lstCoupsPossible[(2*i)+1]].style = """
                    -fx-background-color: $COULEURJ1
                """.trimIndent()
            }
            else{
                vueJeu.plateau.getVueCases()[lstCoupsPossible[2*i]][lstCoupsPossible[(2*i)+1]].style = """
                    -fx-background-color: $COULEURJ2
                """.trimIndent()
            }
        }


        //DEPLACEMENT DES PIONS

        val positionDepart = appli.derniereCase.getCoordonnee()
        val positionArrivee = case.getCoordonnee()
        vueJeu.jeu.deplacer(positionDepart.getX(),positionDepart.getY(),positionArrivee.getX(), positionArrivee.getY())
        appli.derniereCase = case
        appli.derniereCase.setCoordonnee(case.getCoordonnee())
        // IA TEMPORAIRE
        if(vueJeu.jeu.iaActivee() && vueJeu.jeu.getJoueurCourant()!!.getPseudo() == vueJeu.jeu.getNomsJoueurs().second){
            val calculsCoupsSuivants = IA(vueJeu).miniMax(vueJeu.jeu, NOMBRECOUPSDAVANCE,true, Int.MIN_VALUE, Int.MAX_VALUE)
            vueJeu.jeu.deplacer(calculsCoupsSuivants.second[calculsCoupsSuivants.second.size-4], calculsCoupsSuivants.second[calculsCoupsSuivants.second.size-3], calculsCoupsSuivants.second[calculsCoupsSuivants.second.size-2], calculsCoupsSuivants.second[calculsCoupsSuivants.second.size-1])
            if (vueJeu.jeu.getJoueurCourant()!!.getPseudo() == vueJeu.jeu.getNomsJoueurs().second){
                vueJeu.jeu.changeJoueurCourant()
            }
            appli.derniereCase = Case()
            vueJeu.update()
        }

        if (vueJeu.jeu.arretPartie()){
            val vainqueur = vueJeu.jeu.joueurVainqueur()
            if (vainqueur != null) {
                victoire.changeNomJoueur(vainqueur.getPseudo())
            }
            else{
                victoire.changeNomJoueur("personne")
            }
            victoire.show()
            victoire.boutonRejouer.setOnAction {
                vueJeu.contentCentre.children.removeAll(vueJeu.plateau,vueJeu.joueur2)
                val nouveauJeu = Jeu()
                nouveauJeu.initialiserPartie(Joueur(appli.jeu.getNomsJoueurs().first),Joueur(appli.jeu.getNomsJoueurs().second) , NOMBREDECOUPMAX)
                appli.jeu = nouveauJeu
                vueJeu.jeu = nouveauJeu
                vueJeu.plateau = VuePlateau(appli.jeu.getPlateau())

                for (i in 0 until vueJeu.jeu.getPlateau().getTailleHorizontale()){
                    for (j in 0 until vueJeu.jeu.getPlateau().getTailleVerticale()){
                        vueJeu.jeu.getPlateau().getCases()[i][j].setCoordonnee(Coordonnee(i,j))

                        vueJeu.plateau.getVueCases()[i][j].style = """
                    -fx-background-color: $COULEURGRILLE
                """.trimIndent()
                    }
                }
                vueJeu.contentCentre.children.addAll(vueJeu.plateau,vueJeu.joueur2)
                vueJeu.update()

                for (i in vueJeu.plateau.getVueCases().indices){
                    for (j in 0 until vueJeu.plateau.getVueCases()[i].size){
                        vueJeu.plateau.getVueCases()[i][j].addEventHandler(MouseEvent.MOUSE_CLICKED,ControlleurCase(nouveauJeu.getPlateau().getCases()[i][j],vueJeu,stage,scene,appli,victoire ))
                    }
                }

                victoire.close()
            }


        }
        vueJeu.update()
        stage.scene = scene
    }

}