package projet.echecmartien.controleur

import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.scene.Scene
import javafx.scene.image.Image
import javafx.scene.layout.Background
import javafx.scene.layout.BackgroundFill
import javafx.scene.layout.BackgroundImage
import javafx.scene.layout.CornerRadii
import javafx.scene.paint.Paint
import javafx.stage.FileChooser
import javafx.stage.Stage
import projet.echecmartien.librairie.*
import projet.echecmartien.vue.Accessibilite
import java.io.File
import java.lang.Exception

class ControlleurAccessibiliteBackground (vue : Accessibilite) : EventHandler<ActionEvent> {

    var vue : Accessibilite

    init{
        this.vue = vue
    }

    override fun handle(event: ActionEvent?) {

        try {
            val input = FileChooser()
            input.title = "Veuillez saisir le chemin d'accès du fond d'écran : "
            val file = input.showOpenDialog(null)

            BACKGROUND = file.path.toString()

            vue.update()
        }
        catch(e : Exception){
            return
        }


    }
}