package projet.echecmartien.controleur

import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.scene.Scene
import javafx.scene.image.Image
import javafx.scene.layout.Background
import javafx.scene.layout.BackgroundFill
import javafx.scene.layout.BackgroundImage
import javafx.scene.layout.CornerRadii
import javafx.scene.paint.Paint
import javafx.stage.Stage
import projet.echecmartien.librairie.*
import projet.echecmartien.vue.Accessibilite

class ControlleurAccessibiliteDarkMode (vue : Accessibilite) : EventHandler<ActionEvent> {

    var vue : Accessibilite

    init{
        this.vue = vue
    }

    override fun handle(event: ActionEvent?) {
        if (vue.boxDarkMode.isSelected ){
            if (BACKGROUND == "src/main/resources/Nathan.png") {
                BACKGROUND = "src/main/resources/night.png"
            }
            COULEURPOLICE = "white"
            COULEURGRILLE = "#4A297C"
            COULEURGRILLE2 = "#292C7D"
            COULEURFOND = "#3C4255"
            COULEURJ1 = "#8961C6"
            COULEURJ2 = "#5A5ECD"
            FLECHEJOUEUR1 = "src/main/resources/flechejoueur1Dark.png"
            FLECHEJOUEUR2 = "src/main/resources/flechejoueur2Dark.png"


        }else{
            if (BACKGROUND == "src/main/resources/night.png") {
                BACKGROUND = "src/main/resources/Nathan.png"
            }
            COULEURPOLICE = "black"
            COULEURGRILLE = "#F5EFEE"
            COULEURGRILLE2 = "#F1F5E2"
            COULEURFOND = "#F9E4CF"
            COULEURJ1 = "#E1A29B"
            COULEURJ2 = "#ECD49D"
            FLECHEJOUEUR1 = "src/main/resources/flechejoueur1.png"
            FLECHEJOUEUR2 = "src/main/resources/flechejoueur2.png"

        }

        vue.update()
    }
}