package projet.echecmartien.controleur

import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.scene.input.MouseEvent
import javafx.stage.Stage
import javafx.stage.WindowEvent
import projet.echecmartien.AppliJeuEchecMartien
import projet.echecmartien.librairie.COULEURGRILLE
import projet.echecmartien.librairie.COULEURGRILLE2
import projet.echecmartien.librairie.NOMBREDECOUPMAX
import projet.echecmartien.librairie.TAILLEVERTICALE
import projet.echecmartien.modele.Coordonnee
import projet.echecmartien.modele.Jeu
import projet.echecmartien.modele.Joueur
import projet.echecmartien.vue.PseudoJoueur
import projet.echecmartien.vue.Victoire
import projet.echecmartien.vue.VueJeu
import projet.echecmartien.vue.VuePlateau

class ControlleurInitJeu(private val vue: PseudoJoueur, val jeu: Jeu, private val vueJeu: VueJeu, var stage : Stage, var scene: Scene, private var appli: AppliJeuEchecMartien, private val victoire: Victoire): EventHandler<ActionEvent> {
    override fun handle(p0: ActionEvent?) {
        vueJeu.contentCentre.children.removeAll(vueJeu.plateau,vueJeu.joueur2)
        val pseudo = vue.getPseudo()
        if (pseudo.first == pseudo.second){
            vue.labelErreurPseudo.isVisible = true
            return
        }
        vue.labelErreurPseudo.isVisible = false
        val nouveauJeu = Jeu()
        nouveauJeu.initialiserPartie(Joueur(pseudo.first),Joueur(pseudo.second), NOMBREDECOUPMAX)
        appli.jeu = nouveauJeu
        vueJeu.jeu = nouveauJeu
        vueJeu.plateau = VuePlateau(jeu.getPlateau())

        for (i in 0 until vueJeu.jeu.getPlateau().getTailleHorizontale()){
            for (j in 0 until vueJeu.jeu.getPlateau().getTailleVerticale()){
                vueJeu.jeu.getPlateau().getCases()[i][j].setCoordonnee(Coordonnee(i,j))
                if (j < TAILLEVERTICALE /2) {
                vueJeu.plateau.getVueCases()[i][j].style = """
                    -fx-background-color: $COULEURGRILLE
                """.trimIndent()
                }
                else{
                    vueJeu.plateau.getVueCases()[i][j].style = """
                    -fx-background-color: $COULEURGRILLE2
                """.trimIndent()
                }
            }
        }
        vueJeu.contentCentre.children.addAll(vueJeu.plateau,vueJeu.joueur2)
        vueJeu.update()

        for (i in vueJeu.plateau.getVueCases().indices){
            for (j in 0 until vueJeu.plateau.getVueCases()[i].size){
                vueJeu.plateau.getVueCases()[i][j].addEventHandler(MouseEvent.MOUSE_CLICKED,ControlleurCase(nouveauJeu.getPlateau().getCases()[i][j],vueJeu,stage,scene,appli,victoire ))
            }
        }

        stage.scene = scene

    }

}