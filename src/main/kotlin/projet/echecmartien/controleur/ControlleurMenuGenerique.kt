package projet.echecmartien.controleur

import javafx.application.Application
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.scene.input.MouseEvent
import javafx.stage.Stage
import projet.echecmartien.AppliJeuEchecMartien
import projet.echecmartien.librairie.COULEURGRILLE
import projet.echecmartien.librairie.NOMBREDECOUPMAX
import projet.echecmartien.modele.Jeu
import projet.echecmartien.modele.Joueur
import projet.echecmartien.vue.*

class ControlleurMenuGenerique(
    private val menu: MenuGenerique,
    private val stage: Stage,
    private val sceneMenuInitial : Scene,
    private val sceneChargerPartie : Scene,
    private val sceneJoueurVsJoueur : Scene,
    private val sceneJoueurVsIA: Scene,
    val jeu : Jeu,
    val vueJeu : VueJeu,
    val pseudoJoueur: PseudoJoueur,
    val chargerPartie: ChargerPartie,
    val appli : AppliJeuEchecMartien,
    val victoire: Victoire
    ): EventHandler<ActionEvent> {


    override fun handle(p0: ActionEvent?) {
        val src : Bouton = p0?.source as Bouton
        when (src.text) {
            "NOUVELLE PARTIE" -> this.menu.presetNouvellePartie()
            "CHARGER PARTIE" -> {
                chargerPartie.update()
                this.stage.scene = sceneChargerPartie
            }
            "JOUEUR Vs JOUEUR" -> {
                pseudoJoueur.update()
                this.stage.scene = sceneJoueurVsJoueur
            }
            "JOUEUR Vs IA" -> {
                jeu.initialiserPartie(Joueur("JOUEUR"), Joueur("IA"), NOMBREDECOUPMAX)
                jeu.setIA()
                for (i in vueJeu.plateau.getVueCases().indices){
                    for (j in 0 until vueJeu.plateau.getVueCases()[i].size){
                        vueJeu.plateau.getVueCases()[i][j].addEventHandler(MouseEvent.MOUSE_CLICKED,ControlleurCase(jeu.getPlateau().getCases()[i][j],vueJeu,stage,sceneJoueurVsIA,appli,victoire ))
                    }
                }
                for (i in 0 until vueJeu.jeu.getPlateau().getTailleHorizontale()){
                    for (j in 0 until vueJeu.jeu.getPlateau().getTailleVerticale()){
                        vueJeu.plateau.getVueCases()[i][j].style = """
                    -fx-background-color: $COULEURGRILLE
                """.trimIndent()
                    }
                }
                vueJeu.update()

                this.stage.scene = sceneJoueurVsIA
            }
            "RETOUR" -> {
                if (menu.titre.text == "MENU DU JEU") {
                    this.stage.scene = sceneMenuInitial
                } else {
                    this.menu.presetMenuDuJeu()
                }
            }
        }
    }
}