package projet.echecmartien.vue

import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.control.ScrollPane
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.layout.StackPane
import javafx.scene.layout.VBox
import projet.echecmartien.librairie.*
import java.io.FileInputStream

class Regles : ScrollPane(), Vue {

    val retour : Bouton
    val titreLabel : Label
    val preparation : ReglesTitre
    val text1 : Label
    val deroulementDuJeu : ReglesTitre
    val text2 : Label
    val exception : Label
    val text3 : Label
    val finDeLaPartie : ReglesTitre
    val text4 : Label
    val exemple : ImageView
    val texte : VBox

    init {
        val titre = StackPane()
        titre.alignment = Pos.CENTER
        titreLabel = Label("REGLES DU JEU")
        titreLabel.style =("-fx-text-fill: $COULEURBOUTTONRETOUR;-fx-font: 25 arial;-fx-margin: 50%; -fx-font-size: $TAILLEPOLICETITRE")
        titre.children.add(titreLabel)

        texte = VBox()
        preparation = ReglesTitre("Preparation")
        text1 = Label("Disposez les 18 pions comme sur la figure ci-contre. \n" +
                "Un joueur identifie ses pièces par leur position à un instant donné.\n" +
                "Le damier est divisé en 2 zones, une pour chaque joueur. \n" +
                "Toute pièce dans la zone d'un joueur est la sienne.")
        deroulementDuJeu = ReglesTitre("Déroulement du jeu")
        text2 = Label("Chaque joueur, à son tour de jeu, déplace une de ses pièces.\n" +
                "Les grands pions se déplacent verticalement,\n" +
                "horizontalement et diagonalement de n cases (comme la\n" +
                "dame aux Echecs traditionnel).\n" +
                "Les pions moyens se déplacent verticalement,\n" +
                "horizontalement et diagonalement de 1 ou 2 cases.\n" +
                "Les petits pions se déplacent diagonalement de 1 case.\n" +
                "A son tour de jeu un joueur peut déplacer \n" +
                "n'importe quel pion de son camp, soit à l'intérieur\n" +
                "de sa zone soit vers la zone adverse.\t\n")
        exception = Label("exception:")
        exception.style= "-fx-font-weight: bold; -fx-font-style: italic; -fx-font-size: $TAILLEPOLICE"
        text3 = Label("Il est interdit de renvoyer dans la zone adverse un pion qui vient d'arriver dans sa zone. \n" +
                "Mais on peut déplacer ce même pion à l'intérieur de sa zone\n" +
                "\n" +
                "On capture un pion adverse en prenant sa place \n" +
                "(donc fatalement en prenant un pion de sa zone et en allant dans la zone adverse).\n" +
                "Le pion capturé est retiré du damier..\n" +
                "Le saut par dessus un ou n pions adverses ou non n'est pas autorisé.")
        finDeLaPartie = ReglesTitre("Fin de la Partie")
        text4 = Label("Une fois la partie finie \n" +
                "(plus de pions à capturer car ils sont tous capturés ou plus aucunes\n" +
                "prises n'est possibles),\n" +
                "on compte 3 points par grand pion capturés, 2 par moyen et 1 par petit.\n" +
                "\n" +
                "Le gagnant est évidement le joueur qui à le plus de points.\n\n\n")
        retour = Bouton("RETOUR AU MENU", couleur = COULEURBOUTTONRETOUR, couleurTexte = COULEURTEXTEBOUTTONRETOUR)

        text1.style = "-fx-font-size: $TAILLEPOLICE"
        text1.isWrapText = true
        text2.style = "-fx-font-size: $TAILLEPOLICE"
        text2.isWrapText = true
        text3.style = "-fx-font-size: $TAILLEPOLICE"
        text3.isWrapText = true
        text4.style = "-fx-font-size: $TAILLEPOLICE"
        text4.isWrapText = true
        preparation.style = preparation.style + "-fx-font-size: $TAILLEPOLICE"
        deroulementDuJeu.style = deroulementDuJeu.style +"-fx-font-size: $TAILLEPOLICE"
        finDeLaPartie.style = finDeLaPartie.style+"-fx-font-size: $TAILLEPOLICE"

        texte.maxWidth = LARGEURFENETRE*0.8

        exemple = ImageView(Image(FileInputStream("src/main/kotlin/projet/echecmartien/vue/regles.png")))
        exemple.x = texte.width
        exemple.fitWidth = Image(FileInputStream("src/main/kotlin/projet/echecmartien/vue/regles.png")).width*(TAILLEPOLICE)
        exemple.fitHeight = Image(FileInputStream("src/main/kotlin/projet/echecmartien/vue/regles.png")).height*(TAILLEPOLICE)


        texte.children.addAll(titre,preparation,text1,deroulementDuJeu,text2,exception,text3,finDeLaPartie,text4, exemple, retour)
        texte.padding = Insets(25.0,25.0,25.0,25.0)
        texte.alignment = Pos.CENTER
        texte.spacing = TAILLEPOLICE.toDouble()

        this.content = texte

        texte.minWidth = LARGEURFENETRE

    }

    override fun update() {
        texte.style = """
            -fx-background-color: $COULEURFOND;
            -fx-background-size: cover;
        """.trimMargin()
        this.vbarPolicyProperty()
        titreLabel.style =("-fx-text-fill: #F93D3D;-fx-font: 25 arial;-fx-margin: 50%; -fx-font-size: $TAILLEPOLICETITRE")
        text1.style = "-fx-font-size: $TAILLEPOLICE; -fx-text-fill: $COULEURPOLICE"
        text2.style = "-fx-font-size: $TAILLEPOLICE; -fx-text-fill: $COULEURPOLICE"
        text3.style = "-fx-font-size: $TAILLEPOLICE; -fx-text-fill: $COULEURPOLICE"
        text4.style = "-fx-font-size: $TAILLEPOLICE; -fx-text-fill: $COULEURPOLICE"
        preparation.style = """
            -fx-text-fill: #F93D3D;
            -fx-display: block;
            -fx-font-size: 1.17em;
            -fx-margin-top: 1em;
            -fx-margin-bottom: 1em;
            -fx-margin-left: 0;
            -fx-margin-right: 0;
            -fx-font-weight: bold;
            -fx-font-size: $TAILLEPOLICE;
        """.trimIndent()
        exception.style= "-fx-font-weight: bold; -fx-font-style: italic; -fx-font-size: $TAILLEPOLICE; -fx-text-fill: #F93D3D"
        deroulementDuJeu.style = """
            -fx-text-fill: #F93D3D;
            -fx-display: block;
            -fx-font-size: 1.17em;
            -fx-margin-top: 1em;
            -fx-margin-bottom: 1em;
            -fx-margin-left: 0;
            -fx-margin-right: 0;
            -fx-font-weight: bold;
            -fx-font-size: $TAILLEPOLICE;
        """.trimIndent()
        finDeLaPartie.style = """
            -fx-text-fill: #F93D3D;
            -fx-display: block;
            -fx-font-size: 1.17em;
            -fx-margin-top: 1em;
            -fx-margin-bottom: 1em;
            -fx-margin-left: 0;
            -fx-margin-right: 0;
            -fx-font-weight: bold;
            -fx-font-size: $TAILLEPOLICE;
        """.trimIndent()
        exemple.fitWidth = Image(FileInputStream("src/main/kotlin/projet/echecmartien/vue/regles.png")).width*(TAILLEPOLICE/20)
        exemple.fitHeight = Image(FileInputStream("src/main/kotlin/projet/echecmartien/vue/regles.png")).height*(TAILLEPOLICE/20)
        texte.spacing = TAILLEPOLICE.toDouble()
        retour.style = """
            -fx-background-color: $COULEURBOUTTONRETOUR;
            -fx-color: $COULEURTEXTEBOUTTON;
            -fx-border-radius: 2px;
            -fx-border-color: $COULEURBORDERBOUTTON;
            -fx-border-width: 2px;
            -fx-font-weight: bold;
            -fx-font-size: $TAILLEPOLICE;
        """.trimIndent()
        retour.opacity = OPACITY
    }
}

class ReglesTitre(s : String): Label(s){
    init{
        style = """
            -fx-text-fill: #F93D3D;
            -fx-display: block;
            -fx-font-size: 1.17em;
            -fx-margin-top: 1em;
            -fx-margin-bottom: 1em;
            -fx-margin-left: 0;
            -fx-margin-right: 0;
            -fx-font-weight: bold;
            -fx-font-size: $TAILLEPOLICE;
        """.trimIndent()
    }
}