package projet.echecmartien.vue

import javafx.scene.layout.GridPane
import projet.echecmartien.librairie.TAILLEHORIZONTALE
import projet.echecmartien.librairie.TAILLEVERTICALE
import projet.echecmartien.modele.Plateau

class VuePlateau(plateau: Plateau): GridPane() {
    private var vueCases : Array<Array<VueCase>> = Array(TAILLEHORIZONTALE) { Array(TAILLEVERTICALE) { VueCase(null) } }
    init {
        val cases = plateau.getCases()
        for (i in cases.indices) {
            for (j in 0 until cases[i].size) {
                vueCases[i][j].setStyle(i, j)
                vueCases[i][j].setVuePion(cases[i][j])
                add(vueCases[i][j], i, j)
            }
        }
    }
    fun update(plateau: Plateau){
        val cases = plateau.getCases()
        for (i in cases.indices) {
            for (j in 0 until cases[i].size) {
                vueCases[i][j].setVuePion(cases[i][j])
                vueCases[i][j].setStyle(i, j)
            }
        }
    }
    fun getVueCases() = vueCases
}