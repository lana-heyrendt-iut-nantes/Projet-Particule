package projet.echecmartien.vue

import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.control.TextField
import javafx.scene.layout.VBox
import javafx.scene.text.Font
import javafx.scene.text.TextAlignment
import projet.echecmartien.librairie.*

class PseudoJoueur () : VBox(), Vue{
    val inputPseudoJoueur1: TextField
    val inputPseudoJoueur2: TextField
    val bouttonJouer: Bouton
    val bouttonRetour : Bouton
    val labelErreurPseudo : Label
    val labelJCJ : Label
    val boxJoueur1 : VBox
    val boxJoueur2 : VBox
    val labelPseudoJoueur1 : Label
    val labelPseudoJoueur2 : Label
    init{

        labelJCJ = Label("JOUEUR vs JOUEUR")
        labelJCJ.style =("-fx-text-fill: red;-fx-font: $TAILLEPOLICETITRE arial;")

        labelPseudoJoueur1 = Label("Pseudo Joueur 1")
        labelPseudoJoueur2 = Label("Pseudo Joueur 2")
        labelPseudoJoueur1.style =("-fx-text-fill: black;-fx-font: $TAILLEPOLICE arial;")
        labelPseudoJoueur2.style =("-fx-text-fill: black;-fx-font: $TAILLEPOLICE arial;")

        inputPseudoJoueur1 = TextField("Joueur 1")
        inputPseudoJoueur2 = TextField("Joueur 2")
        inputPseudoJoueur1.font = Font("arial", TAILLEPOLICE.toDouble())
        inputPseudoJoueur2.font = Font("arial", TAILLEPOLICE.toDouble())
        inputPseudoJoueur1.maxHeight = (TAILLEPOLICE*2).toDouble()
        inputPseudoJoueur1.maxWidth = (388 + TAILLEPOLICE).toDouble()
        inputPseudoJoueur2.maxHeight = (TAILLEPOLICE*2).toDouble()
        inputPseudoJoueur2.maxWidth = (388 + TAILLEPOLICE).toDouble()

        boxJoueur1 = VBox()
        boxJoueur1.children.addAll(labelPseudoJoueur1, inputPseudoJoueur1)
        boxJoueur1.maxWidth = (388 + TAILLEPOLICE).toDouble()

        boxJoueur2 = VBox()
        boxJoueur2.children.addAll(labelPseudoJoueur2, inputPseudoJoueur2)
        boxJoueur2.maxWidth = (388 + TAILLEPOLICE).toDouble()

        labelErreurPseudo  = Label("Veuillez choisir des pseudos différents.")
        labelErreurPseudo.style =("-fx-text-fill: red;-fx-font: " + (TAILLEPOLICE+13).toString() + " arial;")
        labelErreurPseudo.isVisible = false
        labelErreurPseudo.isWrapText = true
        labelErreurPseudo.textAlignment = TextAlignment.CENTER

        bouttonJouer = Bouton("JOUER",(88 + TAILLEPOLICE).toDouble(), 388.0+ TAILLEPOLICE)
        bouttonJouer.font = Font("arial", TAILLEPOLICE.toDouble())

        bouttonRetour = Bouton("RETOUR", (88 + TAILLEPOLICE).toDouble(), 388.0+ TAILLEPOLICE, COULEURBOUTTONRETOUR, COULEURTEXTEBOUTTONRETOUR)

        this.alignment = Pos.CENTER
        this.spacing = 75.0 - TAILLEPOLICE

        this.children.addAll(labelJCJ, boxJoueur1, boxJoueur2, labelErreurPseudo, bouttonJouer, bouttonRetour)
    }
    fun getPseudo(): Pair<String, String> {
        println(Pair(inputPseudoJoueur1.text, inputPseudoJoueur2.text))
        return Pair(inputPseudoJoueur1.text, inputPseudoJoueur2.text)
    }
    override fun update() {
        this.style = """
            -fx-background-image: url('file:$BACKGROUND');
            -fx-background-size: cover;
        """.trimMargin()

        labelJCJ.style =("-fx-text-fill: red;-fx-font: $TAILLEPOLICETITRE arial;")

        labelPseudoJoueur1.style =("-fx-text-fill: black;-fx-font: $TAILLEPOLICE arial; -fx-text-fill: $COULEURPOLICE;")
        labelPseudoJoueur2.style =("-fx-text-fill: black;-fx-font: $TAILLEPOLICE arial; -fx-text-fill: $COULEURPOLICE;")

        inputPseudoJoueur1.font = Font("arial", TAILLEPOLICE.toDouble())
        inputPseudoJoueur2.font = Font("arial", TAILLEPOLICE.toDouble())
        inputPseudoJoueur1.maxHeight = (TAILLEPOLICE*2).toDouble()
        inputPseudoJoueur1.maxWidth = (388 + TAILLEPOLICE).toDouble()
        inputPseudoJoueur2.maxHeight = (TAILLEPOLICE*2).toDouble()
        inputPseudoJoueur2.maxWidth = (388 + TAILLEPOLICE).toDouble()

        boxJoueur1.maxWidth = (388 + TAILLEPOLICE).toDouble()
        boxJoueur2.maxWidth = (388 + TAILLEPOLICE).toDouble()

        labelErreurPseudo.style =("-fx-text-fill: red;-fx-font: " + (TAILLEPOLICE+13).toString() + " arial;")

        bouttonJouer.style = """
            -fx-background-color: $COULEURFONDBOUTTON;
            -fx-color: $COULEURTEXTEBOUTTON;
            -fx-border-radius: 2px;
            -fx-border-color: $COULEURBORDERBOUTTON;
            -fx-border-width: 2px;
            -fx-font-weight: bold;
            -fx-font-size: $TAILLEPOLICE;
        """.trimIndent()
        bouttonJouer.opacity = OPACITY

        bouttonRetour.style = """
            -fx-background-color: $COULEURBOUTTONRETOUR;
            -fx-color: $COULEURTEXTEBOUTTON;
            -fx-border-radius: 2px;
            -fx-border-color: $COULEURBORDERBOUTTON;
            -fx-border-width: 2px;
            -fx-font-weight: bold;
            -fx-font-size: $TAILLEPOLICE;
        """.trimIndent()
        bouttonRetour.opacity = OPACITY
    }


}