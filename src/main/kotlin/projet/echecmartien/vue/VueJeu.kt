package projet.echecmartien.vue

import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.layout.*
import javafx.scene.paint.Color
import javafx.scene.paint.Paint
import projet.echecmartien.AppliJeuEchecMartien
import projet.echecmartien.librairie.*
import projet.echecmartien.modele.GrandPion
import projet.echecmartien.modele.MoyenPion
import projet.echecmartien.modele.PetitPion



class VueJeu(var appli: AppliJeuEchecMartien) :
        BorderPane(), Vue{
    var jeu =  appli.jeu
    val joueur1 = VBox()
    val joueur2 = VBox()
    var pointsJ1 = VBox()
    var pointsJ2 = VBox()
    var milieu = Pane()
    var jeuCentre = HBox()
    var contentCentre = VBox()
    var plateau = VuePlateau(jeu.getPlateau())
    val panePointJ1 = VBox()
    val panePointJ2 = VBox()
    val header = Pane()
    val footer = Pane()
    val borderLeft = Pane()
    var labelScoreJ1 = Label()
    var labelScoreJ2 = Label()
    var labelJ1 = Label()
    var labelJ2 = Label()
    val chargement = Pane()

    var quitter : Bouton
    val menuDroit = VBox()
    var labelGrandPionJ1 = Label("0")
    var labelMoyenPionJ1 = Label("0")
    var labelPetitPionJ1 = Label("0")
    var labelMoyenPionJ2 = Label("0")
    var labelGrandPionJ2 = Label("0")
    var labelPetitPionJ2 = Label("0")
    val labelCoupSansPriseRestant : Label
    val coupSansPriseRestant : Label
    val labelDernierCoup : Label
    val dernierCoup : Label
    val panelCoupSansPriseRestant : VBox
    var paneFlecheJ1 = Pane()
    var paneFlecheJ2 = Pane()

    init {
        milieu.children.add(jeuCentre)
        plateau.update(jeu.getPlateau())
        paneFlecheJ1.prefHeight = 100.0
        paneFlecheJ1.prefWidth = 100.0
        paneFlecheJ1.opacity = 1.0
        paneFlecheJ2.prefHeight = 100.0
        paneFlecheJ2.prefWidth = 100.0
        paneFlecheJ2.opacity = 0.0


        header.prefHeight = 50.0
        footer.prefHeight = 50.0
        borderLeft.prefWidth = 30.0
        labelJ2 = Label(jeu.getNomsJoueurs().second)
        labelJ1 = Label(jeu.getNomsJoueurs().first)

        panePointJ1.children.addAll(pointsJ1, paneFlecheJ1)
        panePointJ2.children.addAll(paneFlecheJ2, pointsJ2)
        panePointJ2.alignment = Pos.BOTTOM_RIGHT
        contentCentre.children.addAll(joueur1,plateau,joueur2)
        jeuCentre.children.addAll(panePointJ2, contentCentre, panePointJ1)

        //joueur 1 et 2
        joueur2.children.add(labelJ2)
        joueur2.alignment = Pos.CENTER
        labelJ2.style = """
            -fx-font-size: $TAILLEPOLICE;
        """.trimIndent()
        joueur2.border = Border(BorderStroke(Paint.valueOf(COULEURPOLICE.toString()), Paint.valueOf(COULEURPOLICE.toString()), Paint.valueOf(COULEURPOLICE.toString()), Paint.valueOf(COULEURPOLICE.toString()), BorderStrokeStyle.NONE, BorderStrokeStyle.SOLID, BorderStrokeStyle.SOLID, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths(2.0), Insets.EMPTY ))
        joueur2.style = """
            -fx-background-color: $COULEURJ2;
            -fx-color: $COULEURTEXTEBOUTTON
        """.trimIndent()
        joueur2.maxHeight = TAILLECASE*0.75
        joueur2.maxWidth = TAILLECASE*4
        joueur2.prefHeight = TAILLECASE*0.75
        joueur2.prefWidth = TAILLECASE*0.75

        joueur1.children.add(labelJ1)
        joueur1.alignment = Pos.CENTER
        labelJ1.style = """
            -fx-font-size: $TAILLEPOLICE;
        """.trimIndent()
        joueur1.border = Border(BorderStroke(Paint.valueOf(COULEURPOLICE.toString()), Paint.valueOf(COULEURPOLICE.toString()), Paint.valueOf(COULEURPOLICE.toString()), Paint.valueOf(COULEURPOLICE.toString()), BorderStrokeStyle.SOLID, BorderStrokeStyle.SOLID, BorderStrokeStyle.NONE, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths(2.0), Insets.EMPTY ))
        joueur1.style = """
            -fx-background-color: $COULEURJ1;
            -fx-color: $COULEURTEXTEBOUTTON
        """.trimIndent()
        joueur1.maxHeight = TAILLECASE*0.75
        joueur1.maxWidth = TAILLECASE*4
        joueur1.prefHeight = TAILLECASE*0.75
        joueur1.prefWidth = TAILLECASE*4
        setAlignment(joueur1, Pos.TOP_RIGHT )

        //points J2 et J1
        pointsJ2.children.addAll(labelScoreJ2,HBox(VuePetitPion(),labelPetitPionJ2),HBox(VueMoyenPion(),labelMoyenPionJ2),HBox(VueGrandPion(),labelGrandPionJ2))
        pointsJ2.spacing = 10.0
        pointsJ2.padding = Insets(10.0)
        pointsJ2.alignment = Pos.BOTTOM_CENTER
        pointsJ2.maxHeight = TAILLECASE*3.75
        pointsJ2.maxWidth = TAILLECASE*3.75
        pointsJ2.prefHeight = TAILLECASE
        pointsJ2.prefWidth = TAILLECASE
        pointsJ2.style = """
            -fx-background-color: $COULEURJ2;
            -fx-color: $COULEURTEXTEBOUTTON
        """.trimIndent()
        pointsJ2.border = Border(BorderStroke(Paint.valueOf(COULEURPOLICE.toString()), Paint.valueOf(COULEURPOLICE.toString()), Paint.valueOf(COULEURPOLICE.toString()), Paint.valueOf(COULEURPOLICE.toString()), BorderStrokeStyle.SOLID, BorderStrokeStyle.NONE, BorderStrokeStyle.SOLID, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths(2.0), Insets.EMPTY ))

        pointsJ1.children.addAll(HBox(VueGrandPion(),labelGrandPionJ1),HBox(VueMoyenPion(),labelMoyenPionJ1),HBox(VuePetitPion(),labelPetitPionJ1), labelScoreJ1)
        pointsJ1.spacing = 10.0
        pointsJ1.padding = Insets(10.0)
        pointsJ1.alignment = Pos.TOP_CENTER
        pointsJ1.maxHeight = TAILLECASE*3.75
        pointsJ1.maxWidth = TAILLECASE*3.75
        pointsJ1.prefHeight = TAILLECASE*0.75
        pointsJ1.prefWidth = TAILLECASE
        pointsJ1.style = """
            -fx-background-color: $COULEURJ1;
            -fx-color: $COULEURTEXTEBOUTTON
        """.trimIndent()
        pointsJ1.border = Border(BorderStroke(Paint.valueOf(COULEURPOLICE.toString()), Paint.valueOf(COULEURPOLICE.toString()), Paint.valueOf(COULEURPOLICE.toString()), Paint.valueOf(COULEURPOLICE.toString()), BorderStrokeStyle.SOLID, BorderStrokeStyle.SOLID, BorderStrokeStyle.SOLID, BorderStrokeStyle.NONE, CornerRadii.EMPTY, BorderWidths(2.0), Insets.EMPTY ))

        //MENU DROIT
        menuDroit.padding = Insets(12.0)
        quitter = Bouton("Quitter",75.0,150.0)
        menuDroit.children.addAll(quitter,chargement)


        labelCoupSansPriseRestant = Label("Coup sans prise :")
        labelCoupSansPriseRestant.style = ("-fx-text-fill: black;-fx-font: $TAILLEPOLICE arial; -fx-text-fill: $COULEURPOLICE;")
        coupSansPriseRestant = Label("${jeu.getNbCoupSanPrise()} / ${NOMBREDECOUPMAX}")
        coupSansPriseRestant.style = ("-fx-text-fill: black;-fx-font: $TAILLEPOLICE arial; -fx-text-fill: $COULEURBOUTTONRETOUR;")
        labelDernierCoup = Label("Le dernier coup était :")
        dernierCoup = Label("Pas de coup précédent")
        labelDernierCoup.style = labelCoupSansPriseRestant.style
        dernierCoup.style = coupSansPriseRestant.style
        panelCoupSansPriseRestant = VBox()
        panelCoupSansPriseRestant.children.addAll(labelCoupSansPriseRestant, coupSansPriseRestant, labelDernierCoup, dernierCoup)
        panelCoupSansPriseRestant.style = "-fx-background-color: $COULEURFOND"
        panelCoupSansPriseRestant.maxWidth = quitter.maxWidth



        menuDroit.children.add(panelCoupSansPriseRestant)


        right = menuDroit

        left = borderLeft
        center = milieu
        top = header
        bottom = footer

    }
    override fun update(){
        //AFFICHAGE DES FLECHES
        if (jeu.getJoueurCourant()!!.getPseudo() == jeu.getNomsJoueurs().first){
            paneFlecheJ2.opacity = 0.0
            paneFlecheJ1.opacity = 1.0
        }else {
            paneFlecheJ2.opacity = 1.0
            paneFlecheJ1.opacity = 0.0
        }
        paneFlecheJ1.style = """
            -fx-background-image: url('file:$FLECHEJOUEUR1');
            -fx-background-size: cover;
        """.trimMargin()
        paneFlecheJ2.style = """
            -fx-background-image: url('file:$FLECHEJOUEUR2');
            -fx-background-size: cover;
        """.trimMargin()
        labelCoupSansPriseRestant.style = ("-fx-text-fill: black;-fx-font: $TAILLEPOLICE arial; -fx-text-fill: $COULEURPOLICE;")
        dernierCoup.text = "${jeu.getCoordOrg()?.getX()}, ${jeu.getCoordOrg()?.getY()}, vers ${jeu.getCoordDest()?.getX()}, ${jeu.getCoordDest()?.getY()}"
        coupSansPriseRestant.text = ("${jeu.getNbCoupSanPrise()} / ${NOMBREDECOUPMAX}")
        coupSansPriseRestant.style = ("-fx-text-fill: black;-fx-font: $TAILLEPOLICE arial; -fx-text-fill: $COULEURBOUTTONRETOUR;")
        labelDernierCoup.style = labelCoupSansPriseRestant.style
        dernierCoup.style = coupSansPriseRestant.style
        dernierCoup.isWrapText = true
        panelCoupSansPriseRestant.style = "-fx-background-color: $COULEURFOND"

        labelJ1.text = jeu.getNomsJoueurs().first
        labelJ2.text = jeu.getNomsJoueurs().second
        this.jeu = appli.jeu
        labelJ1.style = "-fx-font-size: $TAILLEPOLICE; -fx-text-fill: $COULEURPOLICE"
        labelJ2.style = "-fx-font-size: $TAILLEPOLICE; -fx-text-fill: $COULEURPOLICE"
        labelScoreJ1.style = "-fx-font-size: $TAILLEPOLICE; -fx-text-fill: $COULEURPOLICE"
        labelScoreJ2.style = "-fx-font-size: $TAILLEPOLICE; -fx-text-fill: $COULEURPOLICE"

        joueur2.border = Border(BorderStroke(Paint.valueOf(COULEURPOLICE.toString()), Paint.valueOf(COULEURPOLICE.toString()), Paint.valueOf(COULEURPOLICE.toString()), Paint.valueOf(COULEURPOLICE.toString()), BorderStrokeStyle.NONE, BorderStrokeStyle.SOLID, BorderStrokeStyle.SOLID, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths(2.0), Insets.EMPTY ))
        joueur1.border = Border(BorderStroke(Paint.valueOf(COULEURPOLICE.toString()), Paint.valueOf(COULEURPOLICE.toString()), Paint.valueOf(COULEURPOLICE.toString()), Paint.valueOf(COULEURPOLICE.toString()), BorderStrokeStyle.SOLID, BorderStrokeStyle.SOLID, BorderStrokeStyle.NONE, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths(2.0), Insets.EMPTY ))
        pointsJ2.border = Border(BorderStroke(Paint.valueOf(COULEURPOLICE.toString()), Paint.valueOf(COULEURPOLICE.toString()), Paint.valueOf(COULEURPOLICE.toString()), Paint.valueOf(COULEURPOLICE.toString()), BorderStrokeStyle.SOLID, BorderStrokeStyle.NONE, BorderStrokeStyle.SOLID, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths(2.0), Insets.EMPTY ))
        pointsJ1.border = Border(BorderStroke(Paint.valueOf(COULEURPOLICE.toString()), Paint.valueOf(COULEURPOLICE.toString()), Paint.valueOf(COULEURPOLICE.toString()), Paint.valueOf(COULEURPOLICE.toString()), BorderStrokeStyle.SOLID, BorderStrokeStyle.SOLID, BorderStrokeStyle.SOLID, BorderStrokeStyle.NONE, CornerRadii.EMPTY, BorderWidths(2.0), Insets.EMPTY ))


        this.style = """
            -fx-background-image: url('file:$BACKGROUND');
            -fx-background-size: cover;
        """.trimMargin()
        joueur1.style = """
            -fx-background-color: $COULEURJ1;
            -fx-color: $COULEURTEXTEBOUTTON
        """.trimIndent()
        joueur2.style = """
            -fx-background-color: $COULEURJ2;
            -fx-color: $COULEURTEXTEBOUTTON
        """.trimIndent()
        pointsJ1.style = """
            -fx-background-color: $COULEURJ1;
            -fx-color: $COULEURTEXTEBOUTTON
        """.trimIndent()
        pointsJ2.style = """
            -fx-background-color: $COULEURJ2;
            -fx-color: $COULEURTEXTEBOUTTON
        """.trimIndent()
        contentCentre.children.removeAll(plateau,joueur2)
        plateau.update(jeu.getPlateau())
        contentCentre.children.addAll(plateau,joueur2)
        //MODIFICATION DES SCORES
        labelScoreJ1.text = (jeu.getPlateau().getCases()[0][0].getJoueur()?.calculerScore().toString())
        labelScoreJ2.text = (jeu.getPlateau().getCases()[0][7].getJoueur()?.calculerScore().toString())

        labelGrandPionJ1.style = "-fx-font-size: ${TAILLEPOLICE/2}; -fx-text-fill: $COULEURPOLICE"
        labelMoyenPionJ1.style = "-fx-font-size: ${TAILLEPOLICE/2}; -fx-text-fill: $COULEURPOLICE"
        labelPetitPionJ1.style = "-fx-font-size: ${TAILLEPOLICE/2}; -fx-text-fill: $COULEURPOLICE"
        labelMoyenPionJ2.style = "-fx-font-size: ${TAILLEPOLICE/2}; -fx-text-fill: $COULEURPOLICE"
        labelGrandPionJ2.style = "-fx-font-size: ${TAILLEPOLICE/2}; -fx-text-fill: $COULEURPOLICE"
        labelPetitPionJ2.style = "-fx-font-size: ${TAILLEPOLICE/2}; -fx-text-fill: $COULEURPOLICE"
        if (jeu.getJoueurCourant()!!.getPseudo() == jeu.getNomsJoueurs().first){
            var type = GrandPion().type()
            var cmpt = 0
            for (i in jeu.getJoueurCourant()!!.getPionsCaptures()){ if(i.type() == type){ cmpt ++ } }
            labelGrandPionJ1.text = cmpt.toString()
            cmpt = 0
            type = MoyenPion().type()
            for (i in jeu.getJoueurCourant()!!.getPionsCaptures()){ if(i.type() == type){ cmpt ++ } }
            labelMoyenPionJ1.text = cmpt.toString()
            cmpt = 0
            type = PetitPion().type()
            for (i in jeu.getJoueurCourant()!!.getPionsCaptures()){ if(i.type() == type){ cmpt ++ } }
            labelPetitPionJ1.text = cmpt.toString()
            jeu.changeJoueurCourant()
            cmpt = 0
            type = GrandPion().type()
            for (i in jeu.getJoueurCourant()!!.getPionsCaptures()){ if(i.type() == type){ cmpt ++ } }
            labelGrandPionJ2.text = cmpt.toString()
            cmpt = 0
            type = MoyenPion().type()
            for (i in jeu.getJoueurCourant()!!.getPionsCaptures()){ if(i.type() == type){ cmpt ++ } }
            labelMoyenPionJ2.text = cmpt.toString()
            cmpt = 0
            type = PetitPion().type()
            for (i in jeu.getJoueurCourant()!!.getPionsCaptures()){ if(i.type() == type){ cmpt ++ } }
            labelPetitPionJ2.text = cmpt.toString()
            jeu.changeJoueurCourant()
        }else{
            jeu.changeJoueurCourant()
            var type = GrandPion().type()
            var cmpt = 0
            for (i in jeu.getJoueurCourant()!!.getPionsCaptures()){ if(i.type() == type){ cmpt ++ } }
            labelGrandPionJ1.text = cmpt.toString()
            cmpt = 0
            type = MoyenPion().type()
            for (i in jeu.getJoueurCourant()!!.getPionsCaptures()){ if(i.type() == type){ cmpt ++ } }
            labelMoyenPionJ1.text = cmpt.toString()
            cmpt = 0
            type = PetitPion().type()
            for (i in jeu.getJoueurCourant()!!.getPionsCaptures()){ if(i.type() == type){ cmpt ++ } }
            labelPetitPionJ1.text = cmpt.toString()
            jeu.changeJoueurCourant()
            cmpt = 0
            type = GrandPion().type()
            for (i in jeu.getJoueurCourant()!!.getPionsCaptures()){ if(i.type() == type){ cmpt ++ } }
            labelGrandPionJ2.text = cmpt.toString()
            cmpt = 0
            type = MoyenPion().type()
            for (i in jeu.getJoueurCourant()!!.getPionsCaptures()){ if(i.type() == type){ cmpt ++ } }
            labelMoyenPionJ2.text = cmpt.toString()
            cmpt = 0
            type = PetitPion().type()
            for (i in jeu.getJoueurCourant()!!.getPionsCaptures()){ if(i.type() == type){ cmpt ++ } }
            labelPetitPionJ2.text = cmpt.toString()
        }
    }

}