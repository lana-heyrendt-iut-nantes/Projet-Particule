package projet.echecmartien.vue

import javafx.scene.paint.Paint
import projet.echecmartien.librairie.COULEURPETITPION
import projet.echecmartien.librairie.TAILLECASE

class VuePetitPion(): VuePion(){
    init {
        radius = TAILLECASE *0.25
        fill = Paint.valueOf(COULEURPETITPION)
    }
    fun update(){
        radius = TAILLECASE *0.25
        fill = Paint.valueOf(COULEURPETITPION)
    }
}