package projet.echecmartien.vue

import javafx.geometry.Pos
import javafx.scene.Scene
import javafx.scene.control.Label
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import javafx.stage.Stage
import javafx.stage.StageStyle
import projet.echecmartien.librairie.TAILLEPOLICE

class Victoire(nomJoueurVainqueur: String) : Stage(),Vue {
    val boutonRejouer : Bouton = Bouton("REJOUER",50.0,125.0)
    val boutonRetour : Bouton = Bouton("RETOUR",50.0,125.0)
    private val label : Label = Label("$nomJoueurVainqueur a gagné la partie!!!")
    init {
        initStyle(StageStyle.UNDECORATED)
        label.style = "-fx-font: ${TAILLEPOLICE+4} arial;"
        val pane = VBox()
        pane.alignment = Pos.CENTER
        val paneScene = Scene(pane,400.0,250.0)
        val boutons = HBox()
        pane.spacing = 40.0
        boutons.children.addAll(boutonRetour,boutonRejouer)
        boutons.spacing = 12.0
        boutons.alignment = Pos.BOTTOM_CENTER
        isResizable = false
        pane.children.addAll(label,boutons)
        scene = paneScene
    }
    fun changeNomJoueur(s : String){
        label.text = ("$s a gagné la partie!!!")
    }
    override fun update() {
        return
    }
}