package projet.echecmartien.vue

import javafx.scene.paint.Paint
import projet.echecmartien.librairie.COULEURMOYENPION
import projet.echecmartien.librairie.TAILLECASE

class VueMoyenPion: VuePion(){
    init {
        radius = TAILLECASE *0.35
        fill = Paint.valueOf(COULEURMOYENPION)
    }
    fun update(){
        radius = TAILLECASE *0.25
        fill = Paint.valueOf(COULEURMOYENPION)
    }
}