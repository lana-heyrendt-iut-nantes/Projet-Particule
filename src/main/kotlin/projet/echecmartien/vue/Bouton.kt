package projet.echecmartien.vue

import javafx.scene.control.Button
import projet.echecmartien.librairie.COULEURBORDERBOUTTON
import projet.echecmartien.librairie.COULEURFONDBOUTTON
import projet.echecmartien.librairie.COULEURTEXTEBOUTTON
import projet.echecmartien.librairie.TAILLEPOLICE

class Bouton(str: String, hauteur: Double = 100.0, largeur : Double = 250.0, couleur : String = COULEURFONDBOUTTON, couleurTexte : String = COULEURTEXTEBOUTTON) : Button(str) {
    init{
        style = """
            -fx-background-color: $couleur;
            -fx-color: $couleurTexte;
            -fx-border-radius: 2px;
            -fx-border-color: $COULEURBORDERBOUTTON;
            -fx-border-width: 2px;
            -fx-font-weight: bold;
            -fx-font-size: $TAILLEPOLICE;
        """.trimIndent()
        if (largeur > str.length*TAILLEPOLICE) {
            setMinSize(largeur, hauteur)
        }
        else {
            setMinSize(str.length * TAILLEPOLICE.toDouble(), hauteur)
        }
    }

}