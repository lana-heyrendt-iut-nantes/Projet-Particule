package projet.echecmartien.vue

import javafx.scene.shape.Circle
import projet.echecmartien.librairie.TAILLECASE

abstract class VuePion : Circle() {
    init{
        centerX += TAILLECASE /2
        centerY += TAILLECASE /2
    }

}