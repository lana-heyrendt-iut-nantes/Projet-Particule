package projet.echecmartien.vue

import javafx.geometry.Insets
import javafx.scene.control.Label
import javafx.scene.layout.*
import javafx.scene.paint.Color
import javafx.scene.paint.Paint
import projet.echecmartien.librairie.*
import projet.echecmartien.modele.Case
import projet.echecmartien.modele.GrandPion
import projet.echecmartien.modele.MoyenPion
import projet.echecmartien.modele.PetitPion

class VueCase(case: Case?) : Pane() {
    private var vuePion : VuePion? = null

    init{
        prefHeight = TAILLECASE
        prefWidth = TAILLECASE
        val typeVuePion: VuePion? = when (case?.getPion()) {
            is PetitPion -> VuePetitPion()
            is MoyenPion -> VueMoyenPion()
            is GrandPion -> VueGrandPion()
            else -> null
            }
        if (typeVuePion != null) {
            vuePion = typeVuePion
            children.add(typeVuePion)
        }
    }
    fun setStyle(x: Int, y:Int){
        var colorTop : Paint = Paint.valueOf(COULEURPOLICE.toString())
        if (y == (TAILLEVERTICALE / 2  )) {
            colorTop = Paint.valueOf(COULEURLIGNECENTRALE)
        }
        var colorBot : Paint = Paint.valueOf(COULEURPOLICE.toString())
        if (y == (TAILLEVERTICALE / 2  )-1) {
            colorBot = Paint.valueOf(COULEURLIGNECENTRALE)
        }
        border = Border(BorderStroke(colorTop, Paint.valueOf(COULEURPOLICE.toString()), colorBot, Paint.valueOf(COULEURPOLICE.toString()), BorderStrokeStyle.SOLID, BorderStrokeStyle.NONE, BorderStrokeStyle.NONE, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths(2.0), Insets.EMPTY ))
        if (x == TAILLEHORIZONTALE -1){
            border = Border(BorderStroke(colorTop, Paint.valueOf(COULEURPOLICE.toString()), colorBot, Paint.valueOf(COULEURPOLICE.toString()), BorderStrokeStyle.SOLID, BorderStrokeStyle.SOLID, BorderStrokeStyle.NONE, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths(2.0), Insets.EMPTY ))
        }
        if (y == TAILLEVERTICALE -1){
            border = Border(BorderStroke(Paint.valueOf(COULEURPOLICE.toString()), Paint.valueOf(COULEURPOLICE.toString()), Paint.valueOf(COULEURPOLICE.toString()) , Paint.valueOf(COULEURPOLICE.toString()), BorderStrokeStyle.SOLID, BorderStrokeStyle.NONE, BorderStrokeStyle.SOLID, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths(2.0), Insets.EMPTY ))
        }
        if (x == TAILLEHORIZONTALE -1 && y == TAILLEVERTICALE -1){
            border = Border(BorderStroke(Paint.valueOf(COULEURPOLICE.toString()), Paint.valueOf(COULEURPOLICE.toString()), Paint.valueOf(COULEURPOLICE.toString()) , Paint.valueOf(COULEURPOLICE.toString()), BorderStrokeStyle.SOLID, BorderStrokeStyle.SOLID, BorderStrokeStyle.SOLID, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths(2.0), Insets.EMPTY ))
        }

        if (x == 0){
            val label = Label()
            val lettres = "abcdefgh"
            label.text= lettres[y].toString()
            label.style = "-fx-font-size: 15; -fx-text-fill: $COULEURPOLICE"
            children.add(label)
            label.layoutX+=5
            label.layoutY+=5
        }
        if (y == TAILLEVERTICALE -1){
            val label = Label()
            label.text= (x + 1).toString()
            label.style= "-fx-font-size: 15; -fx-text-fill: $COULEURPOLICE"
            children.add(label)
            label.layoutX +=5
            label.layoutY += TAILLECASE - 20
        }
    }
    fun setVuePion(case: Case?) {
        val typeVuePion: VuePion? = when (case?.getPion()) {
            is PetitPion -> VuePetitPion()
            is MoyenPion -> VueMoyenPion()
            is GrandPion -> VueGrandPion()
            else -> null
        }

        if (vuePion != null){
            children.remove(this.vuePion)
        }
        if (typeVuePion != null) {
            vuePion = typeVuePion
            children.add(typeVuePion)
        }
    }
}