package projet.echecmartien.vue

interface Vue {
    fun update()
}
