package projet.echecmartien.vue

import javafx.geometry.Pos
import javafx.scene.Scene
import javafx.scene.control.Label
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import javafx.stage.Stage
import javafx.stage.StageStyle
import projet.echecmartien.librairie.TAILLEPOLICE

class SauvegarderPartie: Stage(),Vue {
    var boutonSauvegarder : Bouton = Bouton("SAUVEGARDER",50.0,125.0)
    var boutonQuitter : Bouton = Bouton("QUITTER", 50.0, 125.0)
    var boutonRetour : Bouton = Bouton("RETOUR",50.0,125.0)
    private var label : Label = Label("Etes vous sure de vouloir quitter le jeu")
    init {
        initStyle(StageStyle.UNDECORATED)
        label.style = "-fx-font: $TAILLEPOLICE arial;"
        val pane = VBox()
        pane.alignment = Pos.CENTER
        val paneScene = Scene(pane,600.0,250.0)
        val boutons = HBox()
        pane.spacing = 40.0
        boutons.children.addAll(boutonSauvegarder,boutonQuitter,boutonRetour)
        boutons.spacing = 12.0
        boutons.alignment = Pos.BOTTOM_CENTER
        isResizable = false
        pane.children.addAll(label,boutons)
        scene = paneScene
    }

    override fun update() {
        label.style = "-fx-font: $TAILLEPOLICE arial;"
        boutonSauvegarder = Bouton("REJOUER",50.0,125.0)
        boutonRetour = Bouton("RETOUR",50.0,125.0)

    }
}