package projet.echecmartien.vue

import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.layout.BorderPane
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import projet.echecmartien.librairie.*

class Menu : BorderPane(),Vue{
    var nouvellePartie : Bouton
    var regle : Bouton
    var quitter : Bouton
    var accessibilite : Bouton
    private val titreLabel : Label
    private val autre : HBox

    init {

        val menuBoutton = VBox()
        menuBoutton.alignment = Pos.CENTER
        menuBoutton.spacing = 50.0
        titreLabel = Label("MENU DU JEU")
        titreLabel.style =("-fx-text-fill: red;-fx-font: $TAILLEPOLICETITRE arial;")

        nouvellePartie = Bouton("JOUER",88.0+ TAILLEPOLICE, 508.0 + TAILLEPOLICE)
        nouvellePartie.opacity = OPACITY

        autre = HBox()
        autre.spacing = 8.0 + TAILLEPOLICE/3
        autre.alignment = Pos.CENTER

        regle = Bouton("REGLES", 88.0 + TAILLEPOLICE, (250 + TAILLEPOLICE/3).toDouble())
        regle.opacity = OPACITY
        quitter = Bouton("QUITTER", 88.0 + TAILLEPOLICE, (250 + TAILLEPOLICE/3).toDouble(),COULEURBOUTTONRETOUR, COULEURTEXTEBOUTTONRETOUR)
        quitter.opacity = OPACITY
        autre.children.addAll(regle,quitter)
        menuBoutton.children.addAll(titreLabel, nouvellePartie, autre)

        accessibilite = Bouton("Accessibilité",68.0 + TAILLEPOLICE)
        accessibilite.opacity = OPACITY
        setAlignment(accessibilite,Pos.BOTTOM_RIGHT)
        setMargin(accessibilite, Insets(30.0))
        center = menuBoutton
        bottom = accessibilite
    }
    override fun update(){
        this.style = """
            -fx-background-image: url('file:$BACKGROUND');
            -fx-background-size: cover;
        """.trimMargin()
        titreLabel.style =("-fx-text-fill: red;-fx-font: $TAILLEPOLICETITRE arial; -fx-font-color: $COULEURPOLICE")
        autre.spacing = 8.0 + TAILLEPOLICE/3
        nouvellePartie.style = """
            -fx-background-color: $COULEURFONDBOUTTON;
            -fx-color: $COULEURTEXTEBOUTTON;
            -fx-border-radius: 2px;
            -fx-border-color: $COULEURBORDERBOUTTON;
            -fx-border-width: 2px;
            -fx-font-weight: bold;
            -fx-font-size: $TAILLEPOLICE;
            -fx-font-color: $COULEURPOLICE
        """.trimIndent()
        nouvellePartie.opacity = OPACITY
        regle.style = """
            -fx-background-color: $COULEURFONDBOUTTON;
            -fx-color: $COULEURTEXTEBOUTTON;
            -fx-border-radius: 2px;
            -fx-border-color: $COULEURBORDERBOUTTON;
            -fx-border-width: 2px;
            -fx-font-weight: bold;
            -fx-font-size: $TAILLEPOLICE;
            -fx-font-color: $COULEURPOLICE;
        """.trimIndent()
        regle.opacity = OPACITY
        quitter.style = """
            -fx-background-color: $COULEURBOUTTONRETOUR;
            -fx-color: $COULEURTEXTEBOUTTON;
            -fx-border-radius: 2px;
            -fx-border-color: $COULEURBORDERBOUTTON;
            -fx-border-width: 2px;
            -fx-font-weight: bold;
            -fx-font-size: $TAILLEPOLICE;
        """.trimIndent()
        quitter.opacity = OPACITY
        accessibilite.style = """
            -fx-background-color: $COULEURFONDBOUTTON;
            -fx-color: $COULEURTEXTEBOUTTON;
            -fx-border-radius: 2px;
            -fx-border-color: $COULEURBORDERBOUTTON;
            -fx-border-width: 2px;
            -fx-font-weight: bold;
            -fx-font-size: $TAILLEPOLICE;
        """.trimIndent()
        accessibilite.opacity = OPACITY
    }


}