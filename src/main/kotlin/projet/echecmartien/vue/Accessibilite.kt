package projet.echecmartien.vue

import javafx.geometry.Pos
import javafx.scene.control.CheckBox
import javafx.scene.control.ComboBox
import javafx.scene.control.Label
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import projet.echecmartien.librairie.*

class Accessibilite : VBox(), Vue {

    var bouttonRetour : Bouton
    val boxDarkMode : CheckBox
    val boxDaltonien : CheckBox
    val choixPolice : ComboBox<Int>
    var bouttonCharger : Bouton
    var labelTitre : Label = Label("ACCESSIBILITE")
    val labelPolice : Label
    init{
        labelTitre.style =("-fx-text-fill: red;-fx-font: $TAILLEPOLICETITRE arial;")

        boxDarkMode = CheckBox("Mode Sombre")
        boxDarkMode.style = ("-fx-font: $TAILLEPOLICE arial;")

        boxDaltonien = CheckBox("Mode Daltonien")
        boxDaltonien.style = ("-fx-font: $TAILLEPOLICE arial;")

        val boxPolice : HBox = HBox()
        labelPolice = Label("Taille de la police :")
        labelPolice.style = ("-fx-font: $TAILLEPOLICE arial;")
        choixPolice = ComboBox<Int>()
        choixPolice.items.addAll(12,24,36)
        boxPolice.children.addAll(labelPolice, choixPolice)
        boxPolice.alignment = Pos.CENTER

        bouttonCharger = Bouton("Choisir le fond d'écran",(88 + TAILLEPOLICE)/2.toDouble(), (388 + TAILLEPOLICE)/3.toDouble())
        bouttonCharger.opacity = OPACITY

        bouttonRetour = Bouton("RETOUR", (88 + TAILLEPOLICE).toDouble(), 388.0+ TAILLEPOLICE, COULEURBOUTTONRETOUR, COULEURTEXTEBOUTTONRETOUR)
        bouttonRetour.opacity = OPACITY

        this.children.addAll(labelTitre, boxDarkMode, boxDaltonien, boxPolice, bouttonCharger, bouttonRetour)
        this.alignment = Pos.CENTER
        this.spacing = TAILLEPOLICE.toDouble()

    }

    override fun update() {
        this.style = """
            -fx-background-image: url('file:$BACKGROUND');
            -fx-background-size: cover;
        """.trimMargin()
        labelTitre.style =("-fx-text-fill: red;-fx-font: $TAILLEPOLICETITRE arial; -fx-text-fill: $COULEURPOLICE")
        boxDarkMode.style = ("-fx-font: $TAILLEPOLICE arial; -fx-text-fill: $COULEURPOLICE")
        boxDaltonien.style = ("-fx-font: $TAILLEPOLICE arial; -fx-text-fill: $COULEURPOLICE")
        labelPolice.style = ("-fx-font: $TAILLEPOLICE arial; -fx-text-fill: $COULEURPOLICE")
        bouttonCharger.style = """
            -fx-background-color: $COULEURFONDBOUTTON;
            -fx-color: $COULEURTEXTEBOUTTON;
            -fx-border-radius: 2px;
            -fx-border-color: $COULEURBORDERBOUTTON;
            -fx-border-width: 2px;
            -fx-font-weight: bold;
            -fx-font-size: $TAILLEPOLICE;
            -fx-font-color: $COULEURPOLICE
        """.trimIndent()
        bouttonCharger.opacity = OPACITY
        bouttonRetour.style = """
            -fx-background-color: $COULEURBOUTTONRETOUR;
            -fx-color: $COULEURTEXTEBOUTTON;
            -fx-border-radius: 2px;
            -fx-border-color: $COULEURBORDERBOUTTON;
            -fx-border-width: 2px;
            -fx-font-weight: bold;
            -fx-font-size: $TAILLEPOLICE;
            -fx-font-color: $COULEURPOLICE
        """.trimIndent()
        bouttonRetour.opacity = OPACITY
    }

}