package projet.echecmartien.vue

import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.layout.VBox
import projet.echecmartien.librairie.*

class MenuGenerique : VBox(), Vue {

    val titre = Label("")
    val btn1 = Bouton("")
    val btn2 = Bouton("")
    val btn3 = Bouton("", couleur = COULEURBOUTTONRETOUR, couleurTexte = COULEURTEXTEBOUTTONRETOUR)

    init {
        //
        this.alignment = Pos.CENTER
        this.spacing = 50.0
        titre.style = ("-fx-text-fill: red;-fx-font: $TAILLEPOLICETITRE arial;")
        this.children.addAll(titre,btn1,btn2,btn3)
        this.presetMenuDuJeu()
    }

    fun presetMenuDuJeu() {
        this.titre.text = "MENU DU JEU"
        this.btn1.text = "NOUVELLE PARTIE"
        this.btn2.text = "CHARGER PARTIE"
        this.btn3.text = "RETOUR"

    }
    fun presetNouvellePartie() {
        this.titre.text = "NOUVELLE PARTIE"
        this.btn1.text = "JOUEUR Vs JOUEUR"
        this.btn2.text = "JOUEUR Vs IA"
        this.btn3.text = "RETOUR"

    }

    override fun update() {
        this.style = """
            -fx-background-image: url('file:$BACKGROUND');
            -fx-background-size: cover;
        """.trimMargin()
        btn3.style = """
            -fx-background-color: $COULEURBOUTTONRETOUR;
            -fx-color: $COULEURTEXTEBOUTTON;
            -fx-border-radius: 2px;
            -fx-border-color: $COULEURBORDERBOUTTON;
            -fx-border-width: 2px;
            -fx-font-weight: bold;
            -fx-font-size: $TAILLEPOLICE;
        """.trimIndent()
        btn1.style = """
            -fx-background-color: $COULEURFONDBOUTTON;
            -fx-color: $COULEURTEXTEBOUTTON;
            -fx-border-radius: 2px;
            -fx-border-color: $COULEURBORDERBOUTTON;
            -fx-border-width: 2px;
            -fx-font-weight: bold;
            -fx-font-size: $TAILLEPOLICE;
        """.trimIndent()
        btn2.style = """
            -fx-background-color: $COULEURFONDBOUTTON;
            -fx-color: $COULEURTEXTEBOUTTON;
            -fx-border-radius: 2px;
            -fx-border-color: $COULEURBORDERBOUTTON;
            -fx-border-width: 2px;
            -fx-font-weight: bold;
            -fx-font-size: $TAILLEPOLICE;
        """.trimIndent()
        btn3.opacity = OPACITY
        btn2.opacity = OPACITY
        btn1.opacity = OPACITY
        titre.style = ("-fx-text-fill: red;-fx-font: $TAILLEPOLICETITRE arial;")
    }

}