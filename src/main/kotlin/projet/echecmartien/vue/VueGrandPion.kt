package projet.echecmartien.vue

import javafx.scene.effect.DropShadow
import javafx.scene.paint.Color
import javafx.scene.paint.Paint
import projet.echecmartien.librairie.COULEURGRANDPION
import projet.echecmartien.librairie.COULEURPOLICE
import projet.echecmartien.librairie.TAILLECASE

class VueGrandPion: VuePion(){
    init {
        radius = TAILLECASE *0.45
        fill = Paint.valueOf(COULEURGRANDPION)
    }
    fun update() {
        radius = TAILLECASE * 0.25
        fill = Paint.valueOf(COULEURGRANDPION)
    }
}