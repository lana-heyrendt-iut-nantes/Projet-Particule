package projet.echecmartien.vue

import javafx.event.ActionEvent
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.control.TextField
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import javafx.scene.text.Font
import projet.echecmartien.controleur.ControlleurSelectFile
import projet.echecmartien.librairie.*

class ChargerPartie : VBox(), Vue {

    var bouttonRetour : Bouton
    var bouttonValider : Bouton
    val label : Label = Label("Veuillez saisir le chemin d'accès de la partie à charger: ")
    var input = TextField()
    var bouttonSelectionner : Bouton
    val hboxBoutton : HBox
    init{
        label.style =("-fx-text-fill: black;-fx-font: $TAILLEPOLICE arial;")
        label.maxWidth = (388 + TAILLEPOLICE).toDouble()
        label.isWrapText = true
        val hBoxSelecFile = HBox()
        bouttonSelectionner = Bouton("Selectionner",(88 + TAILLEPOLICE)/2.toDouble(), (388 + TAILLEPOLICE)/3.toDouble())
        bouttonSelectionner.addEventHandler(ActionEvent.ACTION,ControlleurSelectFile(this))
        hBoxSelecFile.children.addAll(input, bouttonSelectionner)
        hBoxSelecFile.alignment = Pos.CENTER
        input.font = Font("arial", TAILLEPOLICE.toDouble())
        input.maxHeight = (88 + TAILLEPOLICE)/2.toDouble()
        input.maxWidth = (388 + TAILLEPOLICE).toDouble()

        hboxBoutton = HBox()


        bouttonRetour = Bouton("RETOUR",(88 + TAILLEPOLICE)/2.toDouble(), (388 + TAILLEPOLICE)/3.toDouble(), COULEURBOUTTONRETOUR, COULEURTEXTEBOUTTONRETOUR)
        bouttonValider = Bouton("VALIDER",(88 + TAILLEPOLICE)/2.toDouble(), (388 + TAILLEPOLICE)/3.toDouble())

        hboxBoutton.children.addAll( bouttonRetour, bouttonValider)
        hboxBoutton.alignment = Pos.CENTER
        hboxBoutton.maxWidth = (388 + TAILLEPOLICE).toDouble()
        hboxBoutton.spacing = (388 + TAILLEPOLICE).toDouble() - 2*(388 + TAILLEPOLICE)/3.toDouble()

                this.children.addAll(label, hBoxSelecFile,hboxBoutton)
        this.alignment = Pos.CENTER
        this.spacing = 75.0 - TAILLEPOLICE
    }

    override fun update() {
        this.style = """
            -fx-background-image: url('file:$BACKGROUND');
            -fx-background-size: cover;
        """.trimMargin()
        label.style =("-fx-text-fill: black;-fx-font: $TAILLEPOLICE arial; -fx-text-fill: $COULEURPOLICE;")
        label.maxWidth = (388 + TAILLEPOLICE).toDouble()
        bouttonSelectionner.style = """
            -fx-background-color: $COULEURFONDBOUTTON;
            -fx-color: $COULEURTEXTEBOUTTON;
            -fx-border-radius: 2px;
            -fx-border-color: $COULEURBORDERBOUTTON;
            -fx-border-width: 2px;
            -fx-font-weight: bold;
            -fx-font-size: $TAILLEPOLICE;
            -fx-text-fill: $COULEURPOLICE
        """.trimIndent()
        bouttonSelectionner.opacity = OPACITY
        input.font = Font("arial", TAILLEPOLICE.toDouble())
        input.maxHeight = (88 + TAILLEPOLICE)/2.toDouble()
        input.maxWidth = (388 + TAILLEPOLICE).toDouble()

        bouttonRetour.style = """
            -fx-background-color: $COULEURFONDBOUTTON;
            -fx-color: $COULEURTEXTEBOUTTON;
            -fx-border-radius: 2px;
            -fx-border-color: $COULEURBORDERBOUTTON;
            -fx-border-width: 2px;
            -fx-font-weight: bold;
            -fx-font-size: $TAILLEPOLICE;
            -fx-text-fill: $COULEURPOLICE
        """.trimIndent()
        bouttonRetour.opacity = OPACITY

        bouttonValider.style = """
            -fx-background-color: $COULEURFONDBOUTTON;
            -fx-color: $COULEURTEXTEBOUTTON;
            -fx-border-radius: 2px;
            -fx-border-color: $COULEURBORDERBOUTTON;
            -fx-border-width: 2px;
            -fx-font-weight: bold;
            -fx-font-size: $TAILLEPOLICE;
            -fx-text-fill: $COULEURPOLICE
        """.trimIndent()
        bouttonValider.opacity = OPACITY

        hboxBoutton.maxWidth = (388 + TAILLEPOLICE).toDouble()
        hboxBoutton.spacing = (388 + TAILLEPOLICE).toDouble() - 2*(388 + TAILLEPOLICE)/3.toDouble()

        this.spacing = 75.0 - TAILLEPOLICE

    }


}