package projet.echecmartien

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import projet.echecmartien.librairie.TAILLEHORIZONTALE
import projet.echecmartien.librairie.TAILLEVERTICALE
import projet.echecmartien.modele.*
import java.util.stream.Stream

internal class TestJeu {

    private var jeu : Jeu = Jeu()
    private val joueur1 : Joueur = Joueur("Joueur1")
    private val joueur2 : Joueur = Joueur("Joueur2")

    @BeforeEach
    fun before(){
        jeu = Jeu()
    }
    @Test
    fun arretPartieCoupMax0() {
        jeu.initialiserPartie(joueur1,joueur2,0)
        assertTrue(jeu.arretPartie())
    }

    @Test
    fun arretPartieNotCoupMax1() {
        jeu.initialiserPartie(joueur1,joueur2,1)
        assertFalse(jeu.arretPartie())
    }

    @Test
    fun arretPartieCoupMax1() {
        jeu.initialiserPartie(joueur1,joueur2,1)
        jeu.deplacer(2,2,3,3)
        assertTrue(jeu.arretPartie())
    }

    @Test
    fun changeJoueurCourantInit() {
        jeu.initialiserPartie(joueur1,joueur2,3)
        assertEquals(joueur1, jeu.getJoueurCourant())
    }

    @Test
    fun changeJoueurCourant() {
        jeu.initialiserPartie(joueur1,joueur2,3)
        jeu.deplacer(2,2,3,3)
        assertEquals(joueur2, jeu.getJoueurCourant())
    }


    @ParameterizedTest
    @MethodSource("testdeplacementpossibleProvider")
    // Test pour la fonction Déplacement possible prennant en entrée x,y
    fun testdeplacementPossible(j : Jeu, crd : Coordonnee, res : Boolean, errorMessage : String) {
        assertEquals(res,j.deplacementPossible(crd.getX(),crd.getY()),errorMessage)
    }


    @ParameterizedTest
    @MethodSource("testDeplacementPossible2Provider")
    fun testDeplacementPossible2(
        jeuTest : Jeu,
        crdOrigine : Coordonnee, crdDest : Coordonnee,
        joueur : Joueur?, res : Boolean, ErrorMessage : String
    ) {
        if (joueur != null) {
            assertEquals(res,
                jeuTest.deplacementPossible(crdOrigine.getX(),crdOrigine.getY(),crdDest.getX(),crdDest.getY(),joueur),
                "Le déplacement par le joueur : ${joueur.getPseudo()} sur le plateau : \n ${jeuTest.getPlateau()} " +
                        "d'un pion au coordonnées $crdOrigine vers les coordonnées $crdDest " +
                        "devrait être " + ErrorMessage
            )
        }
    }

    @Test
    fun deplacerImpossible() {
        val jeuSansModif = Jeu()
        jeuSansModif.initialiserPartie(joueur1, joueur2, 1)
        jeu.initialiserPartie(joueur1, joueur2, 1)
        jeu.deplacer(0,0,0,1)
        assertEquals(jeu.getPlateau(),jeuSansModif.getPlateau())
    }

    @Test
    fun deplacerPossible() {
        jeu.initialiserPartie(joueur1, joueur2, 2)
        val pionDepart = jeu.getPlateau().getCases()[2][2].getPion()
        jeu.deplacer(2,2,1,3)
        assertEquals(null,jeu.getPlateau().getCases()[2][2].getPion())
        assertEquals(pionDepart, jeu.getPlateau().getCases()[1][3].getPion())
    }

    @Test
    fun joueurVainqueur() {
        jeu.initialiserPartie(joueur1,joueur2,3)
        val plateau = Plateau()
        var cases = plateau.getCases()
        for (i in 0 until TAILLEHORIZONTALE) {
            for (j in 0 until TAILLEVERTICALE)
                cases[i][j] .setPion(null)
        }
        cases[3][1].setPion(GrandPion())
        plateau.setCases(cases)
        jeu.setPlateau(plateau)
        for (k in 0..2){
            joueur1.ajouterPionCaptures(PetitPion())
            joueur1.ajouterPionCaptures(MoyenPion())
        }
        for (k in 0 until 2){
            joueur1.ajouterPionCaptures(GrandPion())
        }
        assertEquals(joueur1,jeu.joueurVainqueur())

    }

    companion object{
        @JvmStatic
        fun testdeplacementpossibleProvider() : Stream<Arguments?>?{
            val listeArgument : MutableList<Arguments> = mutableListOf()
            // Crée des jeux et les initialise
            val jeuInitial = Jeu()
            jeuInitial.initialiserPartie(Joueur("J1"),Joueur("J2"),5)
            val jeuInitialTourJ2 = Jeu()
            jeuInitialTourJ2.initialiserPartie(Joueur("J1"),Joueur("J2"),5)
            jeuInitialTourJ2.changeJoueurCourant()


            val listeCrd = jeuInitial.getPlateau().GenCoorDonneeAdjacente(Coordonnee(1,1))
            listeCrd.addAll(jeuInitial.getPlateau().GenCoorDonneeAdjacente(Coordonnee(2,6)))

            for (crd in listeCrd) {
                when {
                    crd.getX() in 0..1 && crd.getY() in 0..1 -> {
                        listeArgument.add(Arguments.of(jeuInitial,crd,false,"Le pion en $crd ne devrait pas pouvoir se déplacer"))
                        listeArgument.add(Arguments.of(jeuInitialTourJ2,crd,false, "Le pion en $crd ne devrait pas pouvoir se déplacer (Mauvais coter du plateau"))
                    }
                    crd.getY() == 2 || (crd.getX() == 2 && crd.getY() < 4) -> {
                        listeArgument.add(Arguments.of(jeuInitial,crd,true,"Le pion en $crd devrait pas pouvoir se déplacer"))
                        listeArgument.add(Arguments.of(jeuInitialTourJ2,crd,false, "Le pion en $crd ne devrait pas pouvoir se déplacer (Mauvais coter du plateau"))
                    }
                    crd.getX() in 2..3 && crd.getY() in 6..7 -> {
                        listeArgument.add(Arguments.of(jeuInitial,crd,false, "Le pion en $crd ne devrait pas pouvoir se déplacer (Mauvais coter du plateau"))
                        listeArgument.add(Arguments.of(jeuInitialTourJ2,crd,false,"Le pion en $crd ne devrait pas pouvoir se déplacer"))
                    }
                    crd.getY() == 5 || (crd.getX() == 1 && crd.getY() > 3) -> {
                        listeArgument.add(Arguments.of(jeuInitial,crd,false, "Le pion en $crd ne devrait pas pouvoir se déplacer (Mauvais coter du plateau"))
                        listeArgument.add(Arguments.of(jeuInitialTourJ2,crd,true,"Le pion en $crd devrait pas pouvoir se déplacer"))
                    }
                }
            }
            listeArgument.add(Arguments.of(jeuInitialTourJ2,Coordonnee(-1,-1),false,"Les coordonnées données sont en dehors du plateau, le déplacement devrait être impossible"))
            return Stream.of(*listeArgument.toTypedArray())
        }

        @JvmStatic
        fun testDeplacementPossible2Provider() : Stream<Arguments?>?{
            val listeArgument : MutableList<Arguments> = mutableListOf()

            val jeuInitial = Jeu()
            val listJoueur = listOf(Joueur("J1"),Joueur("J2"))
            jeuInitial.initialiserPartie(listJoueur[0],listJoueur[1],5)

            // Coordonnées invalides
            listeArgument.add( Arguments.of(
                jeuInitial,
                    Coordonnee(-1,-1),Coordonnee(0,0),
                    jeuInitial.getJoueurCourant(),false,
                    "Impossible car les coordonnées d'origine sont invalide"
            ))
            listeArgument.add( Arguments.of(
                jeuInitial,
                Coordonnee(0,0),Coordonnee(-1,-1),
                jeuInitial.getJoueurCourant(),false,
                "Impossible car les coordonnées de destination sont invalide"
            ))
            listeArgument.add( Arguments.of(
                jeuInitial,
                Coordonnee(2,2),Coordonnee(3,3),
                null,false,
                "Impossible car un joueur null ne peut pas déplacer de pion"
            ))
            listeArgument.add( Arguments.of(
                jeuInitial,
                Coordonnee(2,2),Coordonnee(3,3),
                listJoueur[1],false,
                "Impossible le pion à l'origine appartient au mauvais Joueur"
            ))
            val jeuTourJ2 = Jeu()
            jeuTourJ2.initialiserPartie(listJoueur[0],listJoueur[1],5)
            jeuTourJ2.changeJoueurCourant()
            listeArgument.add( Arguments.of(
                jeuTourJ2,
                Coordonnee(1,5),Coordonnee(0,4),
                listJoueur[0],false,
                "Impossible le pion à l'origine appartient au mauvais Joueur"
            ))
            listeArgument.add( Arguments.of(
                jeuInitial,
                Coordonnee(3,0),Coordonnee(3,1),
                listJoueur[0],false,
                "Impossible il n'y a pas de pion à l'origine"
            ))
            listeArgument.add( Arguments.of(
                jeuInitial,
                Coordonnee(1,2),Coordonnee(3,3),
                listJoueur[0],false,
                "Impossible ce déplacement n'existe pas"
            ))
            listeArgument.add( Arguments.of(
                jeuInitial,
                Coordonnee(1,2),Coordonnee(1,3),
                listJoueur[0],false,
                "Impossible les petits pions ne se déplace pas comme ça"
            ))
            listeArgument.add( Arguments.of(
                jeuInitial,
                Coordonnee(0,2),Coordonnee(0,7),
                listJoueur[0],false,
                "Impossible les moyen pions ne se déplace pas comme ça"
            ))
            listeArgument.add( Arguments.of(
                jeuInitial,
                Coordonnee(0,0),Coordonnee(0,7),
                listJoueur[0],false,
                "Impossible un pion ne peut pas passer a travers d'autre pion"
            ))
            listeArgument.add( Arguments.of(
                jeuInitial,
                Coordonnee(2,2),Coordonnee(3,3),
                listJoueur[0],true,
                "possible"
            ))






            return Stream.of(*listeArgument.toTypedArray())
        }
    }
    /*
    val jeuVierge = Jeu()
            jeuVierge.initialiserPartie(listJoueur[0],listJoueur[1],5)
            val newPlateau = jeuVierge.getPlateau()
            newPlateau.videPlateau()

            jeuVierge.setPlateau(newPlateau)
     */

}