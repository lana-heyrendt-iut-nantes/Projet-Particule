package projet.echecmartien

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import projet.echecmartien.modele.Coordonnee
import java.util.StringJoiner
import java.util.stream.Stream

internal class TestCoordonnee {
    private val coordonnee : Coordonnee = Coordonnee(2,3)

    @Test
    fun testX(){
        assertEquals(2, coordonnee.getX())
    }

    @Test
    fun testY(){
        assertEquals(3, coordonnee.getY())
    }

    @ParameterizedTest
    @MethodSource("testEqualsProvider")
    fun testEquals(crd1: Coordonnee,crd2: Coordonnee,oracle : Boolean) {
        assertEquals(oracle,crd1 == crd2,"$crd1,$crd2")
    }

    companion object {
        @JvmStatic
        fun testEqualsProvider() : Stream<Arguments?>? {
            val c = mutableListOf<Coordonnee>(Coordonnee(0,0),Coordonnee(0,1),Coordonnee(1,0),Coordonnee(1,1))
            return Stream.of(
                Arguments.of(c[0],c[0],true),
                Arguments.of(c[0],c[1],false),
                Arguments.of(c[0],c[2],false),
                Arguments.of(c[1],c[2],false)
            )
        }
    }
}