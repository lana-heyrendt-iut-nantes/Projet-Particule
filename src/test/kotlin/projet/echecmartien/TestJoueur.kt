package projet.echecmartien



import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Test
import projet.echecmartien.modele.*

class TestJoueur{

    private val joueur1= Joueur("toto")
    private val petitPion = PetitPion()
    private val moyenPion = MoyenPion()
    private val grandPion = GrandPion()
    private val setVide = mutableListOf<Pion>().toSet()

    @Test
    fun testPseudo() {
        assertEquals("toto", joueur1.getPseudo())
    }

    @Test
    fun getPionsCapturesVide() {
        assertEquals( setVide, joueur1.getPionsCaptures())
    }

    @Test
    fun ajouterPionCapture() {
        joueur1.ajouterPionCaptures(petitPion)
        assertNotEquals(setVide, joueur1.getPionsCaptures())
    }

    @Test
    fun ajouterPionsCaptures() {
        joueur1.ajouterPionCaptures(petitPion)
        joueur1.ajouterPionCaptures(grandPion)
        val set = mutableListOf<Pion>(petitPion, grandPion).toSet()
        assertEquals(set, joueur1.getPionsCaptures())
    }

    @Test
    fun getNbPionsCapturesNull() {
        assertEquals(0, joueur1.getNbPionsCaptures())
    }

    @Test
    fun getNbPionCapture() {
        joueur1.ajouterPionCaptures(petitPion)
        assertEquals(1, joueur1.getNbPionsCaptures())
    }

    @Test
    fun getNbPionsCaptures() {
        joueur1.ajouterPionCaptures(petitPion)
        joueur1.ajouterPionCaptures(grandPion)
        assertEquals(2, joueur1.getNbPionsCaptures())
    }

    @Test
    fun calculerScoreNull() {
        assertEquals(0, joueur1.calculerScore())
    }

    @Test
    fun calculerScorePetitPion() {
        joueur1.ajouterPionCaptures(petitPion)
        assertEquals(1, joueur1.calculerScore())
    }

    @Test
    fun calculerScoreMoyenPion() {
        joueur1.ajouterPionCaptures(moyenPion)
        assertEquals(2, joueur1.calculerScore())
    }

    @Test
    fun calculerScoreGrandPion() {
        joueur1.ajouterPionCaptures(grandPion)
        assertEquals(3, joueur1.calculerScore())
    }

    @Test
    fun calculerScorePlusieursPions() {
        joueur1.ajouterPionCaptures(petitPion)
        joueur1.ajouterPionCaptures(moyenPion)
        joueur1.ajouterPionCaptures(grandPion)
        assertEquals(6, joueur1.calculerScore())
    }

}