package projet.echecmartien


import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import projet.echecmartien.librairie.DeplacementException
import projet.echecmartien.modele.*
import java.util.stream.Stream

internal class TestPion {

    @ParameterizedTest
    @MethodSource("TestgetScoreProvider")
    fun TestgetScore(pion : Pion, expectedScore : Int) {
        assertEquals(
            expectedScore,
            pion.getScore(),
            "Le score du ${pion.type()} pion (${pion.getScore()}) n'est pas égal à $expectedScore")
    }


    @ParameterizedTest
    @MethodSource("TestgetDeplacementProvider")
    fun TestgetDeplacement(pion : Pion, dep: Deplacement, expectedList : List<Coordonnee>) {
        if (expectedList.isEmpty()) {
            org.junit.jupiter.api.assertThrows<DeplacementException> {
                pion.getDeplacement(dep)
            }
        } else {
            assertEquals(
                expectedList,
                pion.getDeplacement(dep),
                "Les coordonnées obtenues par le déplacement d'un ${pion.type()} ne corresponde pas au Déplacement ${dep.getOrigine()} -> ${dep.getDestination()} ")
        }

    }

    companion object {
        @JvmStatic
        fun TestgetScoreProvider() : Stream<Arguments?>? {
            return Stream.of(
                Arguments.of(PetitPion(),1),
                Arguments.of(MoyenPion(),2),
                Arguments.of(GrandPion(),3)
            )
        }

        @JvmStatic
        fun TestgetDeplacementProvider() : Stream<Arguments?>? {
            val listeArgument : MutableList<Arguments> = mutableListOf()

            // Petit pion

            val plateauGenCrd = Plateau()
            plateauGenCrd.initialiser()

            // Test pour tous les types de pions
            for (pion in listOf<Pion>(PetitPion(),MoyenPion(),GrandPion())) {
                // Test tous les déplacements d'une case pour tous les types de pions
                for (crd in plateauGenCrd.GenCoorDonneeAdjacente(Coordonnee(1,1))) {
                    val dep = Deplacement(Coordonnee(1,1),crd)
                    when {
                        dep.estDiagonal() -> { listeArgument.add(Arguments.of(pion, dep, dep.getCheminDiagonal())) }
                        // Lance une exception si le déplacement d'un petit pion n'est pas en diagonal
                        pion is PetitPion -> { listeArgument.add(Arguments.of(pion, dep, mutableListOf<Coordonnee>())) }
                        dep.estVertical() -> { listeArgument.add(Arguments.of(pion, dep, dep.getCheminVertical())) }
                        dep.estHorizontal() -> { listeArgument.add(Arguments.of(pion, dep, dep.getCheminHorizontal())) }
                    }
                }
            }
            // Test les cas spécifique pour les déplacements à plus d'une case
            val dep2cases = Deplacement(Coordonnee(0,0), Coordonnee(0,2))
            val dep5cases = Deplacement(Coordonnee(0,0),Coordonnee(0,5))
            listeArgument.add((Arguments.of(MoyenPion(),dep2cases, dep2cases.getCheminVertical())))
            listeArgument.add((Arguments.of(MoyenPion(),dep5cases, mutableListOf<Coordonnee>())))
            listeArgument.add((Arguments.of(GrandPion(),dep2cases, dep2cases.getCheminVertical())))
            listeArgument.add((Arguments.of(GrandPion(),dep5cases, dep5cases.getCheminVertical())))

            return Stream.of(*listeArgument.toTypedArray())
        }

    }
}

