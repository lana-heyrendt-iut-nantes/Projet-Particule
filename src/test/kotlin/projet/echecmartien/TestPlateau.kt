package projet.echecmartien

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import projet.echecmartien.modele.Coordonnee
import projet.echecmartien.modele.PetitPion
import projet.echecmartien.modele.Plateau
import java.util.stream.Stream

internal class TestPlateau {

    @Test
    fun initialiser() {
        val plateau = Plateau()
        plateau.initialiser()
        assertEquals(" Grand Grand Moyen . Grand Moyen Petit . Moyen Petit Petit . . . . . . . . . . Petit Petit Moyen . Petit Moyen Grand . Moyen Grand Grand", plateau.toString())
    }

    @ParameterizedTest
    @MethodSource("testEqualsProvider")
    fun testEquals(p1: Any?,p2 : Any?, res : Boolean) {
        assertEquals(res,p1 == p2)
    }

    @Test
    fun testPlacePion() {
        val anneHilPlateau = Plateau()
        anneHilPlateau.initialiser()
        anneHilPlateau.placePion(3,3,PetitPion())
        assertEquals(false,anneHilPlateau.getCases()[3][3].estLibre())
    }

    @Test
    fun testVidePlateau() {
        val plateau = Plateau()
        plateau.initialiser()
        plateau.videPlateau()
        for (i in 0 until plateau.getTailleHorizontale()) {
            for (j in 0 until plateau.getTailleVerticale()) {
                assertEquals(true,plateau.getCases()[i][j].estLibre())
            }
        }
    }

    @Test
    fun testGencoord() {
        val p = Plateau()
        p.initialiser()
        assertEquals(mutableListOf<Coordonnee>(),p.GenCoorDonneeAdjacente(Coordonnee(-1,-1)))
    }

    companion object {
        @JvmStatic
        fun testEqualsProvider() : Stream<Arguments?>? {
            val p1 = Plateau()
            p1.initialiser()
            val p2 = Plateau()
            return Stream.of(
                Arguments.of(p1,p1,true),
                Arguments.of(p1,p2,false),
                Arguments.of(p1,null,false),
                Arguments.of(p1,4,false)
            )
        }
    }
}