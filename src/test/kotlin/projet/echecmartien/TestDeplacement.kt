package projet.echecmartien

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import projet.echecmartien.librairie.DeplacementException
import projet.echecmartien.modele.Coordonnee
import projet.echecmartien.modele.Deplacement

internal class TestDeplacement {

    @Test
    fun testIllegalDeplacement(){
        assertThrows<DeplacementException> {
            Deplacement(Coordonnee(0, 0), Coordonnee(1, 2))
        }
    }

    @Test
    fun testEstHorizontal(){
        val deplacement = Deplacement(Coordonnee(0,0), Coordonnee(4,0))
        assertEquals(true, deplacement.estHorizontal())
        assertEquals(false, deplacement.estDiagonal())
        assertEquals(false, deplacement.estVertical())
    }

    @Test
    fun testEstVertical(){
        val deplacement = Deplacement(Coordonnee(0,0), Coordonnee(0,6))
        assertEquals(false, deplacement.estHorizontal())
        assertEquals(false, deplacement.estDiagonal())
        assertEquals(true, deplacement.estVertical())
    }

    @Test
    fun testEstDiagonale1(){
        val deplacement = Deplacement(Coordonnee(0,0), Coordonnee(3,3))
        assertEquals(false, deplacement.estHorizontal())
        assertEquals(true, deplacement.estDiagonal())
        assertEquals(false, deplacement.estVertical())
    }

    @Test
    fun testEstDiagonale2(){
        val deplacement = Deplacement(Coordonnee(2,2), Coordonnee(3,1))
        assertEquals(false, deplacement.estHorizontal())
        assertEquals(true, deplacement.estDiagonal())
        assertEquals(false, deplacement.estVertical())
    }

    //Test Longueur
    @Test
    fun testLongueurHorizontal(){
        val deplacement = Deplacement(Coordonnee(0,0), Coordonnee(4,0))
        assertEquals(4, deplacement.longueur())
    }

    @Test
    fun testLongueurVertical(){
        val deplacement = Deplacement(Coordonnee(0,0), Coordonnee(0,6))
        assertEquals(6, deplacement.longueur())
    }

    @Test
    fun testLongueurDiagonale(){
        val deplacement = Deplacement(Coordonnee(0,0), Coordonnee(3,3))
        assertEquals(3, deplacement.longueur())
    }

    //TestDeplacementPossible
    @Test
    fun testHorizontalPositif(){
        val deplacement = Deplacement(Coordonnee(0,0), Coordonnee(4,0))
        assertEquals(true, deplacement.estHorizontalPositif())
    }

    @Test
    fun testHorizontalNegatif(){
        val deplacement = Deplacement(Coordonnee(4,0), Coordonnee(0,0))
        assertEquals(false, deplacement.estHorizontalPositif())
    }

    @Test
    fun testVerticalPositif(){
        val deplacement = Deplacement(Coordonnee(0,0), Coordonnee(0,6))
        assertEquals(true, deplacement.estVerticalPositif())
    }

    @Test
    fun testVerticalNegatif(){
        val deplacement = Deplacement(Coordonnee(0,6), Coordonnee(0,0))
        assertEquals(false, deplacement.estVerticalPositif())
    }

    @Test
    fun testDiagonalPositifPositif(){
        val deplacement = Deplacement(Coordonnee(2,2), Coordonnee(4,4))
        assertEquals(true, deplacement.estDiagonalPositifXPositifY())
    }

    @Test
    fun testDiagonalPositifNegatif(){
        val deplacement = Deplacement(Coordonnee(2,2), Coordonnee(4,0))
        assertEquals(true, deplacement.estDiagonalPositifXNegatifY())
    }

    @Test
    fun testDiagonalNegatifPositif(){
        val deplacement = Deplacement(Coordonnee(2,2), Coordonnee(0,4))
        assertEquals(true, deplacement.estDiagonalNegatifXPositifY())
    }

    @Test
    fun testDiagonalNegatifNegatif(){
        val deplacement = Deplacement(Coordonnee(2,2), Coordonnee(0,0))
        assertEquals(true, deplacement.estDiagonalNegatifXNegatifY())
    }

    //TestChemin
    @Test
    fun cheminHorizontalPositif(){
        val deplacement = Deplacement(Coordonnee(2,2), Coordonnee(4,2))
        assertEquals(listOf(Coordonnee(3,2), Coordonnee(4,2)),deplacement.getCheminHorizontal())
    }

    @Test
    fun cheminHorizontalNegatif(){
        val deplacement = Deplacement(Coordonnee(2,2), Coordonnee(0,2))
        assertEquals(listOf(Coordonnee(1,2), Coordonnee(0,2)),deplacement.getCheminHorizontal())
    }

    @Test
    fun cheminVerticalPositif(){
        val deplacement = Deplacement(Coordonnee(2,2), Coordonnee(2,4))
        assertEquals(listOf(Coordonnee(2,3), Coordonnee(2,4)),deplacement.getCheminVertical())
    }

    @Test
    fun cheminVerticalNegatif(){
        val deplacement = Deplacement(Coordonnee(2,2), Coordonnee(2,0))
        assertEquals(listOf(Coordonnee(2,1), Coordonnee(2,0)),deplacement.getCheminVertical())
    }

    @Test
    fun cheminDiagonalPositifPositif(){
        val deplacement = Deplacement(Coordonnee(2,2), Coordonnee(4,4))
        assertEquals(listOf(Coordonnee(3,3), Coordonnee(4,4)), deplacement.getCheminDiagonal())
    }

    @Test
    fun cheminDiagonalPositifNegatif(){
        val deplacement = Deplacement(Coordonnee(2,2), Coordonnee(4,0))
        assertEquals(listOf(Coordonnee(3,1), Coordonnee(4,0)), deplacement.getCheminDiagonal())
    }

    @Test
    fun cheminDiagonalNegatifPositif(){
        val deplacement = Deplacement(Coordonnee(2,2), Coordonnee(0,4))
        assertEquals(listOf(Coordonnee(1,3), Coordonnee(0,4)), deplacement.getCheminDiagonal())
    }

    @Test
    fun cheminDiagonalNegatifNegatif(){
        val deplacement = Deplacement(Coordonnee(2,2), Coordonnee(0,0))
        assertEquals(listOf(Coordonnee(1,1), Coordonnee(0,0)), deplacement.getCheminDiagonal())
    }
}