package projet.echecmartien

import org.junit.jupiter.api.Test

import projet.echecmartien.modele.Plateau

internal class TestCase {

    @Test
    fun estLibre() {
        val plateau = Plateau()
        plateau.initialiser()
        val listeCases = plateau.getCases()
        for (i in listeCases.indices) {
            for (j in listeCases[i].indices) {
                val caseOccupee: Boolean = when (Pair(i,j)) {
                    Pair(2,0) -> true
                    Pair(1,1) -> true
                    Pair(0,2) -> true
                    Pair(3,5) -> true
                    Pair(2,6) -> true
                    Pair(1,7) -> true
                    Pair(0,0) -> true
                    Pair(1,0) -> true
                    Pair(0,1) -> true
                    Pair(3,6) -> true
                    Pair(3,7) -> true
                    Pair(2,7) -> true
                    Pair(1,2) -> true
                    Pair(2,2) -> true
                    Pair(2,1) -> true
                    Pair(2,5) -> true
                    Pair(1,6) -> true
                    Pair(1,5) -> true

                    else -> false
                }
                if (caseOccupee && listeCases[i][j].estLibre()){
                    throw (Exception("La case n'est pas libre"))
                }
            }
        }
    }


}